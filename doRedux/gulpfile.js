var gulp = require('gulp');
var webpack = require('gulp-webpack');

gulp.task('css', function () {
      return gulp.src('./css/project-overview-web.css')
        .pipe(gulp.dest('../css'));
});

gulp.task('js', function() {
	return gulp.src('./client/index.js')
	  	.pipe(webpack( require('./webpack.config.js') ))
	  	.pipe(gulp.dest('../js'));
});

gulp.task('default', function() {
  gulp.start('js', 'css');
})
