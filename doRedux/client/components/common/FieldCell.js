import React, { Component } from 'react';

const FieldCell = (props) => {
    return(
        <div className = 'row' style={{margin:2}}>
            <div className = 'col-md-8 col-sm-8 col-xs-8' style={{textAlign:'right'}}>
                <label style={{fontWeight:'400'}}>{props.param} :</label>
            </div>
            <div className = 'col-md-4 col-sm-4 col-xs-4' style={{textAlign:'left',overflowWrap:'break-word'}}>
                <label style={{fontWeight:'400'}}>{props.value}</label>
            </div>
        </div>
    )
}

module.exports = FieldCell
