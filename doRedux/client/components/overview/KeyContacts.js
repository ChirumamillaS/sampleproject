import React, { Component } from 'react';
import FieldCell from '../common/FieldCell'
import { Grid, Row, Col, Button, Modal, DropdownButton,MenuItem } from 'react-bootstrap';
import { updateContacts, UPDATE_CONTACTS } from '../../actions';
import store from '../../store';
import { connect } from 'react-redux';

const CSRStr = 'Customer Service Representative'

const ContactCard = (props) =>{
    return (

        <div className='text-left row' style={{padding:10}}>
            <div className = 'col-md-2 col-lg-2'>
                <div className = 'center-block'>
                    <div className = 'col-md-4'>
                        <span className='glyphicon glyphicon-user' style={{fontSize:20}}/>
                    </div>
                </div>
            </div>
            <div id= 'contacts-overview-number' className = 'col-md-10 col-lg-10'>
                <label style={{fontWeight:'400'}}>{props.contacInfo.name}</label>
                <label style={{fontWeight:'400'}}>{props.contacInfo.contact_type}</label>
                <label style={{fontWeight:'400'}}>{props.contacInfo.email}</label>
                <label style={{fontWeight:'400'}}>{props.contacInfo.phone}</label>
            </div>
        </div>
    )
}

const ModalContactCard = (props) =>{
    var currentCSRName = ''
    for(var i in props.modifiedContactsList){
        if(props.modifiedContactsList[i].contact_type == CSRStr){
            currentCSRName =  props.modifiedContactsList[i].name
        }
    }
    return (
        <div style={{marginTop:'20px'}}>
            <div className='col-md-3'>
                <span className='glyphicon glyphicon-user' style={{fontSize:20}}/>
            </div>
            <div className='col-md-9' style={{textAlign:'left'}}>
                <div>
                    { props.isCSR === '1' && props.contactInfo.contact_type == CSRStr ?
                        <select onChange={(e)=>{props.handleChangeCSR(e.target.value)}} value={currentCSRName}>
                            {
                                props.CSRList.map((eachCSR,i)=>{
                                    return <option key={i} value={eachCSR.name}>{eachCSR.name}</option>
                                })
                            }
                        </select> :
                        <p style={{fontWeight:'400'}}> {props.contactInfo.name} </p>
                    }
                </div>
                <div><p style={{fontWeight:'400'}}> {props.contactInfo.contact_type} </p></div>
                <div><p style={{fontWeight:'400'}}> {props.contactInfo.email} </p></div>
                <div>
                    {
                        (props.isCSR === '1' || props.contactInfo.contact_type_id == 16) && props.contactInfo.contact_type != 'Hotline' ?
                            <input id='contacts-overview-edit-box' value={props.contactInfo.phone} style={{fontWeight: '400', border: '1px solid black' }}
                                   onChange={(e) => {
                                       props.handlePhNumberEdit(e.target.value, props.contactInfo.user_id)
                                   }}>

                            </input>:
                            <label style={{fontWeight:'400'}}>{props.contactInfo.phone}</label>
                    }
                </div>
            </div>
        </div>
    )
}


class KeyContacts extends Component {
    constructor(props){
        super(props)
        this.CSRList =[]
        this.state = {
            showModal: false,
            contacts : [],
            alteredContacts : [],
            currentCSR : {},
            newCSR : {},
            CSRs : {},
            modifiedContactsList : []
        }

        this.handleSave  = this.handleSave.bind(this);
        this.handlePhNumberEdit = this.handlePhNumberEdit.bind(this);
        this.handleChangeCSR = this.handleChangeCSR.bind(this);
        this.handleDismiss = this.handleDismiss.bind(this);
    }

    handleChangeCSR(newCSRName){
        //debugger
        var modifiedContactsList = this.state.modifiedContactsList
        //var initialName = modifiedContactsList[3].name
        for(var i in modifiedContactsList){
            if(modifiedContactsList[i].contact_type == CSRStr){
                for(var j in this.CSRList) {
                    if(this.CSRList[j].name == newCSRName) {
                        modifiedContactsList[i] = this.CSRList[j]
                    }
                }
            }
        }

        this.setState({modifiedContactsList})
    }

    componentWillMount(){
        const contacts = JSON.parse(JSON.stringify(this.props.info))
        const modifiedContactsList = JSON.parse(JSON.stringify(this.props.info))

        //ReFormatting the CSR LIST as the Object Structure does not the Match the List of Contacts
        for(var key in this.props.csrInfo){
            this.CSRList.push({
                contact_order:this.props.csrInfo[key].contact_order || 2,
                contact_type: CSRStr,
                email:this.props.csrInfo[key].email,
                img:this.props.csrInfo[key].img || '',
                name:this.props.csrInfo[key].first_name + ' '+ this.props.csrInfo[key].last_name,
                phone:this.props.csrInfo[key].meta.meta_value,
                user_id:this.props.csrInfo[key].id || '',
                contact_type_id : '15'
            })
        }

        this.setState({
            contacts: contacts,
            modifiedContactsList: modifiedContactsList
        });

    }

    handlePhNumberEdit(changed_phone, user_id){
        var updatedContacts = this.state.modifiedContactsList;

        updatedContacts.map((contact, i) => {
            if ( contact.user_id == user_id ) {
                var newText = '';
                var numbers = '0123456789';
                var is_delete = contact.phone.length > changed_phone.length;

                for ( var i = 0; i < changed_phone.length; i++ ){
                    if ( numbers.indexOf(changed_phone[i]) > -1 ) {
                        newText = newText + changed_phone[i];
                        if ( !is_delete ){
                            if( newText.length === 3 || newText.length === 7 ) {
                                newText+= '-';
                            }
                            if( newText.length > 12 ) {
                                newText = newText.replace(/[-]/gm, '');
                            }
                        }
                    }
                }
                contact.phone = newText;
            }
        })

        this.setState({
            modifiedContactsList: updatedContacts
        });
    }

    handleDismiss(){
        const origContactsList = this.state.contacts;
        this.setState({modifiedContactsList: origContactsList})
    }

    handleSave(){
        fetch('/sbapi/projects/contacts', {
                method: 'PATCH',
                headers: {'Content-type': 'application/json'},
                credentials: 'include',
                body: JSON.stringify( { 'contacts': this.state.modifiedContactsList } )
            }
        )
            .then(function(response){
                return response.json();
            })
            .then((data)=>{
                this.setState({
                    contacts: data['contacts']
                });

                let action = {type: UPDATE_CONTACTS,
                    data: data['contacts']};
                store.dispatch(action);
            }).catch((error)=>{

        });
        this.setState({
            showModal:false
        });
    }


    render() {
        return (
            <div id='overview-contacts-panel' className='container-fluid' style={{background:'white'}}>
                <Modal id="qa-contact-modal" show={this.state.showModal} onHide={this.close}>
                    <div style={{textAlign:'center'}}>
                        <div className = 'row center' style={{padding:'10px'}}>
                            <div id= 'contacts-overview-edit' className= 'col-md-10 col-sm-10 col-xs-10' style={{textAlign:'left'}}>
                                <label className='qa-header' style={{fontWeight:'400',fontFamily:'calibri',fontSize:'18px',color:'grey'}}>EDIT CONTACTS</label>
                            </div>
                            <div className= 'col-md-2 col-sm-2 col-xs-2'>
                                <span id= 'overview-contacts-dismiss' className='glyphicon glyphicon-remove-sign'  style= {{fontSize:20}} onClick={()=>{this.setState({showModal:false}),this.handleDismiss()}}/>
                            </div>
                        </div>
                        <div style={{height:'1.5px',background:'lightgrey',marginLeft:'10px',marginRight:'10px'}}/>
                        <div className='row'>
                            <div className='col-md-6 row' style={{marginLeft:'15px'}}>
                                {
                                    this.state.modifiedContactsList.map((eachContact,i)=> {
                                        if(i < this.state.modifiedContactsList.length/2){
                                            return  <ModalContactCard handlePhNumberEdit = {this.handlePhNumberEdit} contactInfo = {eachContact} key ={i} isCSR = { this.props.is_csr } CSRList = {this.CSRList} handleChangeCSR = {this.handleChangeCSR} modifiedContactsList = {this.state.modifiedContactsList}/>
                                        }
                                    })
                                }
                            </div>
                            <div className='col-md-6 row'>
                                {
                                    this.state.modifiedContactsList.map((eachContact,j)=> {
                                        if(j > this.state.modifiedContactsList.length/2){
                                            return  <ModalContactCard handlePhNumberEdit = {this.handlePhNumberEdit} contactInfo = {eachContact} key ={j} isCSR = { this.props.is_csr } CSRList = {this.CSRList}  handleChangeCSR = {this.handleChangeCSR} modifiedContactsList = {this.state.modifiedContactsList}/>
                                       }
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <Modal.Footer>
                        <div className = 'row'>
                            <div className='col-md-10'>

                            </div>
                            <div id= 'overview-contacts-save' className='col-md-2'>
                                <Button onClick={this.handleSave}>Save</Button>
                            </div>
                        </div>
                    </Modal.Footer>
                </Modal>
                <div id= 'overview-contacts-main-panel' className = 'row' style={{paddingTop:'10px'}}>
                    <div id= 'overview-contacts-header' className= 'col-md-10 col-sm-10 col-xs-10' style={{textAlign:'left'}}>
                        <label className='qa-header' style={{fontWeight:'400',fontFamily:'calibri',fontSize:'18px',color:'grey'}}>KEY CONTACTS </label>
                    </div>
                    <div id= 'overview-contacts-pencil'  className= 'col-md-2 col-sm-2 col-xs-2'>
                        <span className='glyphicon glyphicon-pencil' onClick={()=>{this.setState({showModal:true})}}/>
                    </div>
                </div>
                <div style={{height:'1.5px',background:'lightgrey',width:'100%'}}/>
                {
                    this.state.contacts.map((item, i)=>{
                        return <ContactCard key={ i } contacInfo={ item }/>
                    })
                }
            </div>
        );
    }
}

module.exports = KeyContacts
