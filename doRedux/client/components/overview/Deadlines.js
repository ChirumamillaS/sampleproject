import React, { Component } from "react";
import { Grid, Row, Col, Modal } from "react-bootstrap";

const FieldCell = (props) => {
    var key = props.param.charAt(0).toUpperCase() + props.param.slice(1)
    {/* Formatting Date from yyyy-dd-mm to mm/dd/yyyy*/}
    var value = props.value
    if(props.value){
        var iDate = new Date(props.value);
        var value = (iDate.getMonth() + 1) + "/" + iDate.getDate() + "/" +  iDate.getFullYear()
    }
    return(
        <div className = "row" style={{margin:2}}>
            <div className = "col-md-8 col-sm-8 col-xs-8" style={{textAlign:"right"}}>
                <label style={{fontWeight:"400"}}>{} {key} :</label>
            </div>
            <div className = "col-md-4 col-sm-4 col-xs-4" style={{textAlign:"left",overflowWrap:"break-word"}}>
                <label style={{fontWeight:"400"}}>{value}</label>
            </div>
        </div>
    )
}

class Deadlines extends Component {
    constructor(props){
        super(props)
        this.state = {
            showModal: false
        }
    }
    render() {
        var convertedArr =[]
        for(var key in this.props.info){
            convertedArr.push({key: key, value : this.props.info[key]})
        }
        // Seprating Pages from Cover and Endsheet To add "Pages" string nect to number of pages
        var Pages = []
        var coverInfo = []

        for(var key in convertedArr){
            if(convertedArr[key].key != "cover" &&  convertedArr[key].key != "endsheet") {
                Pages.push(convertedArr[key])
            }else {
                coverInfo.push(convertedArr[key])
            }
        }

        return (

            <div id= "deadlines-overview-main-panel" className="container-fluid" style={{background:"white"}}>
                <div className = "row" style={{paddingTop:"10px"}}>
                   <div id= "deadlines-header" className= "col-md-12 col-sm-12 col-xs-12" style={{textAlign:"left"}}>
                        <label className= "qa-header" style={{fontWeight:"400",fontFamily:"calibri",fontSize:"18px",color:"grey"}}>DEADLINES</label>
            </div>
        </div>
        <div style={{height:"1.5px",background:"lightgrey",width:"100%"}}/>
            <div style={{paddingTop:"10px",paddingBottom:"10px"}}>
        {/* if No Dealines are available UI show No Deadlines Message */}
        {
            coverInfo.length ?
                coverInfo.map((item,i)=>{
                    return <FieldCell param={item.key} value={item.value} key = {i}/>}) :null
        }
            {
                Pages.length ?
                    Pages.map((item,i)=>{
                    return <FieldCell param={item.key+ " Pages"} value={item.value} key = {i}/>}) :
                        <div> <label style={{fontSize:"15px",color:"lightgrey",fontWeight:"300"}}> No Deadlines Available </label> </div>
            }
                </div>
            </div>
        );
    }
}

module.exports = Deadlines