import React, { Component } from "react";
import { Grid, Row, Col, MenuItem,Button, Modal } from "react-bootstrap";
import { updateSchool, UPDATE_SCHOOL } from "../../actions";
import store from "../../store";
import "../../../../components-web/multi-select/multi-select.less";
import MultiSelectKeyValue from "../../../../components-web/multi-select/multiselect_keyvalue.js";



var gradesLookUp = []
const translateTo = function(convertType,grades){
    if(convertType == 'idsToNames') {
        var gradeNames = []
        for(var i in grades){
            for(var j in gradesLookUp) {
                if( grades[i] === gradesLookUp[j].id ){
                    gradeNames.push( gradesLookUp[j].grade )
                }
            }
        }
        return gradeNames
    }else {
        var gradeIds = []
        for( var i in grades ){
            for( var j in gradesLookUp ) {
                if( grades[i] === gradesLookUp[j].grade ){
                    gradeNames.push( gradesLookUp[j].id )
                }
            }
        }
        return gradeIds
    }
}


const FieldCell = (props) => {
    var value = props.value
    if( props.param === "Grades" ){
        value = JSON.stringify( translateTo('idsToNames',props.value)).slice(1,-1).replace(/\"/g,"").replace(/,/g,", ")

    }
    return(
        <div className = "row" style={ { margin:2 } }>
            <div className = "col-md-8 col-sm-8 col-xs-8" style={ { textAlign:"right"} }>
                <label style={ { fontWeight:"400" } }>{ props.param } :</label>
            </div>
            <div className = "col-md-4 col-sm-4 col-xs-4" style={ { textAlign:"left",overflowWrap:"break-word" } }>
                <label style={ { fontWeight:"400" } }>{ value }</label>
            </div>
        </div>
    )
}

class ModalView extends Component {
    constructor(props){
        super(props)
        this.state = {
            showModal: "false",
            modified_grades: []
        }
    }

    componentWillMount(){
        this.setState({
            modified_grades: this.props.modalInfo.grades.value
        })
    }

    componentWillReceiveProps(newProps){
        this.setState({
            modified_grades: newProps.modalInfo.grades.value
        })
    }

    render(){
        var modalInfo = []
        for( var key in this.props.modalInfo ){
            modalInfo.push( this.props.modalInfo[key] )
        }
        return (
            <Modal id="qa-school-modal" show={this.props.showModal}>
                <div style={ { textAlign:"center" } }>
                    <div className = "row center" style={ { padding:"10px" } }>
                        <div id="school-overview-edit" className= "col-md-10 col-sm-10 col-xs-10" style={ { textAlign:"left" } }>
                            <label style={ { fontWeight:"400",fontFamily:"calibri",fontSize:"18px",color:"grey" } }>EDIT SCHOOL</label>
                        </div>
                        <div className= "col-md-2 col-sm-2 col-xs-2">
                            <span className="glyphicon glyphicon-remove-sign"  style= { { fontSize:20 } } onClick={ ()=>{ this.props.dismissModal() } }/>
                        </div>
                    </div>
                    <div style={ { height:"1.5px",background:"lightgrey",marginLeft:"10px",marginRight:"10px" } }/>
                        <div className ="row" style={ { marginTop:10 } }>
                            <div style={ { paddingTop:"20px",paddingBottom:"20px" } }>
                                <div className="col-md-6" style={{textAlign:"right"}}>
                                    <label style={ { fontWeight:"400" } }>Grades :</label>
                                </div>
                                <div id="school-overview-grades" className="col-md-6" style={ { textAlign:"left",color:"black" } }>
                                    <MultiSelectKeyValue items={ this.props.availGrades } key_name={ "id" } value_name={ "grade" } selected = { this.state.modified_grades } onChange={ ( grades ) =>{ this.setState ( { modified_grades:grades } ) } } />
                                </div>
                                <div className="col-md-1">
                            </div>
                        </div>
                    {
                        modalInfo.map( ( eachInfo,i )=> {
                            return <ModelCell lValue = { { label: eachInfo.key, disable:false } } rValue = { { label: eachInfo.value, disable:true } }/>
                    }
                )
            }
            </div>
        </div>
        <Modal.Footer>
            <div className = "row">
                <div className="col-md-10">
            </div>
            <div id="school-overview-save" className="col-md-2">
                <Button onClick={ () =>{ this.props.handleSave ( this.state.modified_grades ) } }>Save</Button>
            </div>
        </div>
        </Modal.Footer>
        </Modal>
    )
    }
}

const ModelCell = ( props ) => {
    return(
        <div style={ { paddingTop:"20px",paddingBottom:"10px" } }>
            <div className="col-md-6" style={ { textAlign:"right",color: props.lValue.disable ? "lightgrey" : "black" } }>
                <label style={ { fontWeight:"400"} } > { props.lValue.label }</label>
            </div>
                <div className="col-md-5" style={{textAlign:"left",color: props.rValue.disable ? "lightgrey" : "black" }}>
                    <label style={ { fontWeight:"400"} } > { props.rValue.label }</label>
                </div>
                    <div className="col-md-1">
                </div>
            </div>
        )
    }

class SchoolDetail extends Component {
    constructor(props){
        super(props)
        this.state = {
            showModal: false,
        }
        this.handleSave = this.handleSave.bind(this)
        this.dismissModal = this.dismissModal.bind(this)
        gradesLookUp = this.props.grades
    }

    dismissModal(){
        this.setState({
            showModal:false
        })
    }

    handleSave(UpdatedGrades){
        fetch("/sbapi/projects/school", {
            method: "PATCH",
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json"
            },
            credentials: "include",
            body: JSON.stringify({ grades: UpdatedGrades })
        })
            .then(function(response){
                return response.json();
            })
            .then( (data) =>{
        var updatedInfo = this.state.schoolInfo
        updatedInfo.grades.value = data.school.grades
        this.setState({
            schoolInfo : updatedInfo
        })
    })
    .catch((error)=>{

        })
        this.setState( { showModal: false } );
    }
    formateData(props){
        var schoolInfo = {
            schoolName : {
                key:"School Name",
                value:props.school_name
            },
            grades : {
                key:"Grades",
                value:props.grades
            },
            billingAddress: {
                key: "Billing Address",
                value: props.billing_address_1 + " "+  props.billing_address_2 +" "+  props.billing_address_3
            },
            shippingAddress :{
                key: "Shipping Address",
                value: props.shipping_address_1 +" "+  props.shipping_address_2 +" "+  props.shipping_address_3
            },
            phone: {
                key:"Phone",
                value:props.school_phone
            },
            salesRegion : {
                key:"Sales Region",
                value:props.sales_region
            },
            salesOffice :{
                key:"Sales Office",
                value:props.sales_office
            }
        }

        return schoolInfo
    }

    componentWillMount(){
        this.setState({
            schoolInfo : this.formateData( this.props.info )
        })
    }

    componentWillReceiveProps(newProps){
        this.setState({
            schoolInfo : this.formateData( newProps.info )
        })
    }


    render() {
        var schoolInfo = []
        for(var key in this.state.schoolInfo){
            schoolInfo.push( this.state.schoolInfo[key] )
        }
        return (
            <div id="school-overview-main-panel" className="container-fluid" style={ { background:"white" } }>
                <ModalView showModal={ this.state.showModal } modalInfo = { JSON.parse ( JSON.stringify( this.state.schoolInfo ) ) } availGrades = { this.props.grades } dismissModal = { this.dismissModal } handleSave = { this.handleSave }/>
                    <div className = "row" style={ { paddingTop:"10px" } }>
                        <div id="school-overview-panel" className= "col-md-10 col-sm-10 col-xs-10" style={{textAlign:"left"}}>
                            <label className="qa-header" style={ { fontWeight:"400",fontFamily:"calibri",fontSize:"18px",color:"grey" } }>SCHOOL</label>
                        </div>
                        <div id ="school-overview-pencil" className= "col-md-2 col-sm-2 col-xs-2">
                            <span className="glyphicon glyphicon-pencil" onClick={ () =>{ this.setState ( { showModal:true } ) } }/>
                        </div>
                    </div>
                    <div style={ { height:"1.5px",background:"lightgrey",width:"100%" } }/>
                        <div style={ { paddingTop:"10px",paddingBottom:"10px" } }>
                    {
                        schoolInfo.map ( ( eachInfo,i )=> {
                            return <FieldCell param={ eachInfo.key } value={ eachInfo.value } key = { i }/>
                        })
                    }
            </div>
        </div>
    );
    }
}

module.exports = SchoolDetail
