import React, { Component } from "react";
import FieldCell from "../common/FieldCell"
import { Grid, Row, Col, Modal,Button } from "react-bootstrap";

const InfoPanel = (props) => {
    if(props.title != "ADDITIONAL ITEMS"){
        if(props.rows.length == 0 ){
            return null
        }
        return(
            <div style={{paddingTop:"20px"}}>
                <div>
                    <p style={{fontWeight:"400",fontSize:"15px",marginLeft:"10px",color:"grey"}}>{props.title}</p>
                </div>
                <div style={{height:"1px",margin:"0px 10px 10px 10px",background:"lightgrey"}}/>
                <div  style={{textAlign:"center"}}>
                    {
                        props.rows.map((eachRow,i)=>
                                <div className="row" style={{paddingBottom:"8px"}} key={i}>
                                    <div className="col-md-6" style={{textAlign:"right"}}>{eachRow.key} :</div>
                                    <div className="col-md-6" style={{textAlign:"left"}}>{eachRow.value}</div>
                                </div>
                        )
                    }
                </div>
            </div>
        )
    }

    return (
        <div style={{paddingTop:"20px"}}>
            <div>
                <p style={{fontWeight:"400",fontSize:"15px",marginLeft:"10px",color:"grey"}}>{props.title}</p>
            </div>
            <div style={{height:"1px",margin:"0px 10px 10px 10px",background:"lightgrey"}}/>
            <div  style={{textAlign:"center"}}>
                <p style={{fontSize:"15px",color:"grey"}}> No Additional Items</p>
            </div>
        </div>
    )

}

class BookSpec extends Component {
    constructor(props){
        super(props)
        this.YearbookInfo = []
        this.CoverInfo = []
        this.EndSheetInfo = []
        this.EndSheetBackInfo = []
        this.field_order = [
            "copies",
            "personalization_type",
            "requested_ship_date",
            "yearbook_software"
        ]

        this.field_translations = {
            "copies": "Copies",
            "personalization_type": "Personalization Type",
            "requested_ship_date": "Requested Ship Date",
            "yearbook_software": "Yearbook Software",
        };
        this.state = {
            showModal: false
        }
    }

    componentWillMount () {
        if(!Array.isArray(this.props.info.details)){

            if( typeof this.props.info.details["2017 YEARBOOK"] == "undefined") {
                this.YearbookInfo = []
            }else {
                this.YearbookInfo = [
                    {
                        key: "level of Specification",
                        value: this.props.info.details["2017 YEARBOOK"].level_of_specification
                    },
                    {
                        key: "Product Line",
                        value: this.props.info.details["2017 YEARBOOK"].product_line
                    },
                    {
                        key: "Trim Size",
                        value: this.props.info.details["2017 YEARBOOK"].trim_size
                    },
                    {
                        key: "Delivery Season",
                        value: this.props.info.details["2017 YEARBOOK"].delivery_season
                    },
                    {
                        key: "Page Preparation",
                        value: this.props.info.details["2017 YEARBOOK"].page_preparation
                    },
                    {
                        key: "Operating System",
                        value: this.props.info.details["2017 YEARBOOK"].operating_system
                    },
                    {
                        key: "Images Scanned by",
                        value: this.props.info.details["2017 YEARBOOK"].images_scanned_by
                    },
                    {
                        key: "Copy Submission Method",
                        value: this.props.info.details["2017 YEARBOOK"].copy_submission_method
                    },
                    {
                        key: "Page Count",
                        value: this.props.info.details["2017 YEARBOOK"].page_count
                    },
                    {
                        key: "Customer Order Quantity",
                        value: this.props.info.details["2017 YEARBOOK"].customer_order_quantity
                    },
                    {
                        key: "Number of Copies",
                        value: this.props.info.details["2017 YEARBOOK"].number_of_copies
                    },
                    {
                        key: "Binding Type",
                        value: this.props.info.details["2017 YEARBOOK"].binding_type
                    },
                    {
                        key: "Round and Back",
                        value: this.props.info.details["2017 YEARBOOK"].round_and_back
                    },
                    {
                        key: "Head Bands",
                        value: this.props.info.details["2017 YEARBOOK"].headbands
                    },
                    {
                        key: "Contents paper",
                        value: this.props.info.details["2017 YEARBOOK"].contents_paper
                    },
                    {
                        key: "Autograph Section",
                        value: this.props.info.details["2017 YEARBOOK"].autograph_selection
                    },
                    {
                        key: "YearZine",
                        value: this.props.info.details["2017 YEARBOOK"].yearzine
                    },
                    {
                        key: "Proofs",
                        value: this.props.info.details["2017 YEARBOOK"].Proofs
                    },
                    {
                        key: "Convert Spot Colors",
                        value: this.props.info.details["2017 YEARBOOK"].convert_spot_colors
                    },
                    {
                        key: "Number of stiff Covers",
                        value: this.props.info.details["2017 YEARBOOK"].number_of_stiff_covers
                    },
                    {
                        key: "Personalization",
                        value: this.props.info.details["2017 YEARBOOK"].personalization
                    },
                    {
                        key: "Names Plate Color",
                        value: this.props.info.details["2017 YEARBOOK"].names_plate_color
                    },
                    {
                        key: "Endsheeet Design Type",
                        value: this.props.info.details["2017 YEARBOOK"].endsheet_desigtn_type
                    },
                    {
                        key: "Endsheeet Stock",
                        value: this.props.info.details["2017 YEARBOOK"].endsheet_proof
                    },
                    {
                        key: "Endsheet Decoration Locations",
                        value: this.props.info.details["2017 YEARBOOK"].endsheet_decoration_locations
                    },
                    {
                        key: "Back Endsheet Stock",
                        value: this.props.info.details["2017 YEARBOOK"].back_endsheet_stock
                    },
                    {
                        key: "Quantity for Endsheet Run",
                        value: this.props.info.details["2017 YEARBOOK"].quantity_for_endsheet_run
                    },
                    {
                        key: "Preparation Plant",
                        value: this.props.info.details["2017 YEARBOOK"].preparation_plant
                    }
                ]
            }
            if( typeof this.props.info.details["COVER-HD-2017"] == "undefined") {
                this.CoverInfo = []
            }else {

                this.CoverInfo = [
                    {
                        key: "Cover Type",
                        value: this.props.info.details["COVER-HD-2017"].cover_type
                    },
                    {
                        key: "Number of Stiff Covers",
                        value: this.props.info.details["COVER-HD-2017"].number_of_stiff_covers
                    },
                    {
                        key: "Binder Boards",
                        value: this.props.info.details["COVER-HD-2017"].binder_boards
                    },
                    {
                        key: "Hinge Size (inches)",
                        value: this.props.info.details["COVER-HD-2017"].hinge_size_inches
                    },
                    {
                        key: "Case Setup(mm)",
                        value: this.props.info.details["COVER-HD-2017"].case_setup_mm
                    },
                    {
                        key: "Cover Material Family",
                        value: this.props.info.details["COVER-HD-2017"].cover_material_family
                    },
                    {
                        key: "Special Material Family",
                        value: this.props.info.details["COVER-HD-2017"].special_material_family
                    },
                    {
                        key: "Materail Cut Size(mm)",
                        value: this.props.info.details["COVER-HD-2017"].material_cut_size_mm
                    },
                    {
                        key: "Omit Balfour Logo",
                        value: this.props.info.details["COVER-HD-2017"].omit_balfour_logo
                    },
                    {
                        key: "Design Type",
                        value: this.props.info.details["COVER-HD-2017"].design_type
                    },
                    {
                        key: "Spine Copy",
                        value: this.props.info.details["COVER-HD-2017"].spine_copy
                    },
                    {
                        key: "Back Lid Decoration",
                        value: this.props.info.details["COVER-HD-2017"].back_lid_decoration
                    },
                    {
                        key: "Decoration Option 1",
                        value: this.props.info.details["COVER-HD-2017"].decoration_option_1
                    },
                    {
                        key: "Slikscreen Color",
                        value: this.props.info.details["COVER-HD-2017"].slikscreen_color
                    },
                    {
                        key: "Decoration Option 2",
                        value: this.props.info.details["COVER-HD-2017"].decoration_option_2
                    },
                    {
                        key: "Cover Proof",
                        value: this.props.info.details["COVER-HD-2017"].cover_proof
                    },
                    {
                        key: "Add 1 Instructions",
                        value: this.props.info.details["COVER-HD-2017"].add1_instructions
                    }
                ]
            }


            if( typeof this.props.info.details["ENDSHEET-2017"] == "undefined") {
                this.EndSheetInfo = []
            }else {
                this.EndSheetInfo =  [
                    {
                        key: "Design Type",
                        value: this.props.info.details["ENDSHEET-2017"].design_type
                    },
                    {
                        key: "Decoration Location",
                        value: this.props.info.details["ENDSHEET-2017"].decoration_location
                    },
                    {
                        key: "Endsheet Stock",
                        value: this.props.info.details["ENDSHEET-2017"].endsheet_stock
                    },
                    {
                        key: "Decoration Option 1",
                        value: this.props.info.details["ENDSHEET-2017"].decoration_option_1
                    },
                    {
                        key: "Endsheet Proof",
                        value: this.props.info.details["ENDSHEET-2017"].endsheet_proof
                    },
                    {
                        key: "Plate/Press Type",
                        value: this.props.info.details["ENDSHEET-2017"].plate_press_type
                    }
                ]
            }

            if( typeof this.props.info.details["ENDSHEET-BK-2017"] == "undefined") {
                this.EndSheetBackInfo = []
            }else {
                this.EndSheetBackInfo = [
                    {
                        key: "Design Type",
                        value: this.props.info.details["ENDSHEET-BK-2017"].design_type
                    },
                    {
                        key: "Decoration Location",
                        value: this.props.info.details["ENDSHEET-BK-2017"].decoration_location
                    },
                    {
                        key: "Endsheet Stock",
                        value: this.props.info.details["ENDSHEET-BK-2017"].endsheet_stock
                    },
                    {
                        key: "Decoration Option 1",
                        value: this.props.info.details["ENDSHEET-BK-2017"].decoration_option_1
                    },
                    {
                        key: "Endsheet Proof",
                        value: this.props.info.details["ENDSHEET-BK-2017"].endsheet_proof
                    },
                    {
                        key: "ES-BK Press Color",
                        value: this.props.info.details["ENDSHEET-BK-2017"].es_color
                    },
                    {
                        key: "Back Endsheet Prep Plant",
                        value: this.props.info.details["ENDSHEET-BK-2017"].back_endsheet_prep_plant
                    }
                ]
            }
        }
    }
    render() {
        var info = this.props.info
        return (
            <div id="bookspec-overview-panel" className="container-fluid" style={{background:"white"}}>
                <Modal id= "qa-bookspec-modal" bsSize="large" aria-labelledby="contained-modal-title-sm" show ={this.state.showModal} onHide = {()=>{this.setState({showModal:false})}}>
                    <Modal.Header closeButton >
                        <Modal.Title id="contained-modal-title-sm">BOOK SPECIFICATION</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {  (!Array.isArray(this.props.info.details)) ?
                            <div className="row">
                                <div className ="col-md-6">
                                    <InfoPanel title = "YEARBOOK" rows = {this.YearbookInfo}/>
                                    <InfoPanel title = "ADDITIONAL ITEMS" rows = {[]}/>
                                </div>
                                <div className ="col-md-6">
                                    <InfoPanel title = "COVER" rows = {this.CoverInfo}/>
                                    <InfoPanel title = "ENDSHEET" rows = {this.EndSheetInfo}/>
                                    <InfoPanel title = "ENDSHEET - BK" rows = {this.EndSheetBackInfo}/>
                                </div>
                            </div> :
                            <div> NO INFORMATION AVAILABLE</div>
                        }
                    </Modal.Body>
                </Modal>
                <div id="bookspec-overview-main-panel" className = "row" style={{paddingTop:"10px"}}>
                    <div id="bookspec-overview-header" className= "col-md-9 col-sm-10 col-xs-10" style={{textAlign:"left"}}>
                        <label className="qa-header" style={{fontWeight:"400",fontFamily:"calibri",fontSize:"18px",color:"grey"}}>BOOK SPECIFICATION</label>
                    </div>
                    <div id="bookspec-overview-link" className= "col-md-3 col-sm-2 col-xs-2" onClick={()=>{this.setState({showModal:true})}}>
                        <label style={{color:"lightblue",fontWeight:"400"}}> (View All) </label>
                    </div>
                </div>
                <div style={{height:"1.5px",background:"lightgrey",width:"100%"}}/>
                <div style={{paddingTop:"10px",paddingBottom:"10px"}}>
                    {
                        this.field_order.map(function (key) {
                            if (typeof(info[key]) == "object") {
                                return null
                            } else {
                                if (key in this.field_translations) {
                                    return <FieldCell param={this.field_translations[key]} value={info[key]} key={key}/>
                                }
                                else {
                                    return <FieldCell param={key} value={info[key]} key={key}/>
                                }
                            }
                        }.bind(this))
                    }
                </div>
            </div>
        );
    }
}

module.exports = BookSpec