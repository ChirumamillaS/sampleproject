import React, { Component } from "react";
// Re-usable component. Please check the component on how to use.
import FieldCell from "../common/FieldCell"
import { Grid, Row, Col } from "react-bootstrap";

class BookSales extends Component {
    constructor(props) {
        super(props)

        this.field_order = [
            "total_orders",
            "online",
            "on_campus",
            "total",
            "available",
            "exact_quantity"
        ]

        this.field_translations = {
            "total_orders": "Total Orders",
            "online": "Online",
            "on_campus": "On Campus",
            "total": "Total",
            "available": "Available",
            "exact_quantity": "Exact Quantity",
        };

        this.state = {
            showModal: false
        }
    }

    render() {
        var info = this.props.info;
        let sales = {
            "total_orders": 0,
            "online": 0,
            "on_campus": 0,
            "total": 0,
            "available": 0,
            "exact_quantity": 0 
        }
        let origin = info.booksPerOrigin;
        if (origin){
            sales["total_orders"] = info.orders;
            sales["online"] = origin["SmartPay"] + origin["Balfour.com"];
            sales["on_campus"] = origin["On Campus"];
            sales["total"] = sales["online"]+sales["on_campus"];
        }
        return (
            <div id="sales-oveview-main-panel" className="container-fluid" style={{background:"white"}}>
                <div id="sales-oveview-panel" className = "row" style={{paddingTop:"10px"}}>
                    <div id="sales-overview-header" className= "col-md-6 col-sm-10 col-xs-10" style={{textAlign:"left"}}>
                        <label className="qa-header" style={{fontWeight:"400",fontFamily:"calibri",fontSize:"18px",color:"grey"}}>BOOK SALES</label>
                    </div>
                    <div id="sales-overview-link" className= "col-md-6 col-sm-2 col-xs-2">
                        <label style={{color:"lightblue",fontWeight:"400"}}> <a href="/sales-orders/sales-dashboard">(View Sales Dashboard)</a> </label>
                    </div>
                </div>
                <div style={{height:"1.5px",background:"lightgrey",width:"100%"}}/>
                <div style={{paddingTop:"10px",paddingBottom:"10px"}}>
                    {
                        this.field_order.map(function (key) {
                            if (key in this.field_translations) {
                                return <FieldCell param={this.field_translations[key]} value={sales[key]} key={key}/>
                            }
                            else {
                                return <FieldCell param={key} value={sales[key]} key={key}/>
                            }
                        }.bind(this))
                    }
                </div>
            </div>
        );
    }
}

module.exports = BookSales
