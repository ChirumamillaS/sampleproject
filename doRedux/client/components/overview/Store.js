import React, { Component } from "react";
import { Modal } from "react-bootstrap"
import FieldCell from "../common/FieldCell"

const SalesDetailHeader = (props) => {
    return (
        <div className="container-fluid" id="store-overview"style={{marginTop:"10px"}}>
            <div className = "row">
                <div className = "col-md-4 col-xs-4" style={{textAlign:"left"}}>
                    <p style={{fontWeight:"400"}}>Store type : {props.info.store_type}</p>
            </div>
            <div className = "col-md-3 col-xs-3" style={{textAlign:"left"}}>
                <p style={{fontWeight:"400"}}>Sales Tax : {props.info.sales_tax}</p>
            </div>
            <div className = "col-md-1 col-xs-1">
                <p style={{fontWeight:"400"}}>Sales Start Date</p>
            </div>
            <div className = "col-md-1 col-xs-1">
                <p style={{fontWeight:"400"}}>{props.info.sales_start}</p>
            </div>
            <div className = "col-md-1 col-xs-1">
                <p style={{fontWeight:"400"}}></p>
            </div>
            <div className = "col-md-1 col-xs-1">
                <p style={{fontWeight:"400"}}>Final Sales Date</p>
            </div>
            <div className = "col-md-1 col-xs-1">
                <p style={{fontWeight:"400"}}>{props.info.final_sales}</p>
            </div>
        </div>
    </div>
)
}

const Headers = () => {
    return (
        <div className="container-fluid" style={{textAlign:"center",marginTop:"10px",background:"lightgrey",verticleAlign:"middle"}}>
            <div className = "row">
                <div className = "col-md-4 col-xs-5" style={{textAlign:"left"}}>
                    <p style={{fontWeight:"bold"}}>Product Name</p>
                </div>
                <div className = "col-md-3 col-xs-3" style={{textAlign:"left"}}>
                    <p style={{fontWeight:"bold"}}>Grade</p>
                </div>
                <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"bold"}}>Sales Price</p>
                </div>
                <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"bold"}}>Final Price</p>
                </div>
                <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"bold"}}>Start</p>
                </div>
                    <div className = "col-md-1 col-xs-1">
                        <p style={{fontWeight:"bold"}}>End</p>
                </div>
            </div>
        </div>
    )
}

const EachYearBookPanel = (props) => {
    var tiers = []
    for(var key in props.info.tiers){
        tiers.push(props.info.tiers[key])
    }
    var namestamping = props.info.namestamping || []
    return (
        <div className="container-fluid" style={{textAlign:"center",verticleAlign:"middle",marginLeft:"20px"}}>
            <div className = "row">
                <div className = "col-md-4 col-xs-4" style={{textAlign:"left"}}>
                    <p style={{fontWeight:"400",color:"lightblue"}}>{props.info.name}</p>
                </div>
                <div className = "col-md-3 col-xs-4" style={{textAlign:"left"}}>
                    <p style={{fontWeight:"400"}}>{props.info.grades}</p>
                </div>
                    <div className = "col-md-1 col-xs-1">
                        <p style={{fontWeight:"400"}}>{props.info.consumer_price}</p>
                </div>
                <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"400"}}></p>
                </div>
                    <div className = "col-md-1 col-xs-1">
                        <p style={{fontWeight:"400"}}>{props.info.sales_start_date}</p>
                </div>
                    <div className = "col-md-1 col-xs-1">
                        <p style={{fontWeight:"400"}}>{props.info.sales_end_date}</p>
                </div>
            </div>
                {
                    tiers.map((eachTire, i)=> {
        return (
            <div className = "row" key ={i}>
                <div className = "col-md-4 col-xs-4" style={{textAlign:"left"}}>
                    <p style={{fontWeight:"400",marginLeft:"40px"}}>{eachTire.name}</p>
                </div>
                    <div className = "col-md-3 col-xs-3" style={{textAlign:"left"}}>
                        <p style={{fontWeight:"400"}}>{eachTire.grades}</p>
                    </div>
                        <div className = "col-md-1 col-xs-1">
                            <p style={{fontWeight:"400"}}>{eachTire.consumer_price}</p>
                    </div>
                        <div className = "col-md-1 col-xs-1">
                            <p style={{fontWeight:"400"}}></p>
                    </div>
                        <div className = "col-md-1 col-xs-1">
                            <p style={{fontWeight:"400"}}></p>
                    </div>
                        <div className = "col-md-1 col-xs-1">
                            <p style={{fontWeight:"400"}}>{eachTire.sales_start_date.slice(0,10)}</p>
                    </div>
                    <div className = "col-md-1 col-xs-1">
                        <p style={{fontWeight:"400"}}></p>
                </div>
            </div>
        )
    })
}
        <div className = "row">
            <div className = "col-md-4 col-xs-4" style={{textAlign:"left"}}>
                <p style={{fontWeight:"400"}}>{ namestamping.length ? "Namestamping ( Up to " + namestamping.length + " )" : ""}</p>
            </div>
                <div className = "col-md-3 col-xs-3" style={{textAlign:"left"}}>
                    <p style={{fontWeight:"400"}}>{namestamping.length ? namestamping[0].grades:""}</p>
            </div>
                <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"400"}}>{namestamping.length ? namestamping[0].price:""}</p>
            </div>
                <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"400"}}></p>
            </div>
                <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"400"}}></p>
            </div>
                 <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"400"}}></p>
            </div>
                <div className = "col-md-1 col-xs-1">
                    <p style={{fontWeight:"400"}}></p>
            </div>
        </div>
    </div>
    )
}

const YearBookPanel = (props) => {
    if(props.info.length == 0){
        return null
    }
    return (
        <div>
            <div style={{display:"block",paddingTop:"20px",textAlign:"left",margin:0}}>
                <span className="glyphicon glyphicon-minus" style={{fontSize:"10px",color:"grey"}}></span>
                    <label style={{marginLeft:"10px",color:"grey"}}>{props.headerTitle}</label>
                </div>
    {
        props.info.map((eachSchool,i) => {
        return (
            <div style={{paddingBottom:"15px"}} key={i}>
                <div style={{height:"1.5px",background:"lightgrey"}}/>
                    <div style={{marginTop:"10px"}}>
                        <EachYearBookPanel info = {eachSchool}/>
                </div>
            </div>
        )
    })
}
</div>
    )
}

class Store extends Component {
    constructor(props){
        super(props)
        this.state = {
            showModal: false
        }
    }
    render() {
        return (
            <div className="container-fluid" id="store-overview-panel" style={{background:"white"}}>
                <Modal id="qa-store-modal" show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton onClick={()=>{this.setState({showModal:false})}}>
                        <Modal.Title>Project Model</Modal.Title>
                    </Modal.Header>
                <Modal.Body>
                    <h4>This is a Project Model</h4>
                </Modal.Body>
            <Modal.Footer>
            </Modal.Footer>
        </Modal>
        <div className = "row" id="store-overview-main-panel" style={{paddingTop:"10px"}}>
            <div className= "col-md-10 col-sm-10 col-xs-10" style={{textAlign:"left"}}>
                <p id="store-overview-link" className="qa-header" style={{fontWeight:"400",fontFamily:"calibri",fontSize:"20px",color:"grey"}}><a href={ this.props.school.store_url } id="store-overview-link" target="_blank">STORE (View Online Store)</a></p>
        </div>
        <div className= "col-md-2 col-sm-2 col-xs-2">
            <span className="glyphicon glyphicon-pencil" onClick={()=>{this.setState({showModal:true})}}/>
            </div>
        </div>
        <div style={{height:"1.5px",background:"lightgrey",width:"100%"}}/>
            <div className="container-fluid">
                <SalesDetailHeader info ={this.props.info}/>
            </div>
            <div className="container-fluid">
            <Headers/>
                </div>
                <div className="container-fluid">
                    <YearBookPanel headerTitle = {"Yearbook and Personizations"} info = {this.props.products.yearbooksandpersonalizations}/>
                    <YearBookPanel headerTitle = {"Pakages"} info = {this.props.products.packages}/>
                    <YearBookPanel headerTitle = {"Accessories"} info = {this.props.products.accessories}/>
                    <YearBookPanel headerTitle = {"Ads"} info = {this.props.products.ads}/>
                    <YearBookPanel headerTitle = {"Custom"} info = {this.props.products.custom}/>
                </div>
            </div>
        );
    }
}

module.exports = Store