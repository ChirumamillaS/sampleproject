import React, { Component } from "react";
import { Modal } from "react-bootstrap"
import FieldCell from "../common/FieldCell"

class Mail extends Component {
    constructor(props){
        super(props)
        this.state = {
            showModal: false
        }
    }
    render() {
        var info = this.props.info;
        return (
            <div id= "overview-mail-panel"  className="container-fluid" style={{background:"white"}}>
                <Modal id="qa-mail-modal" show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton onClick={()=>{this.setState({showModal:false})}}>
                        <Modal.Title>Project Model</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h4>This is a Mail Model</h4>
                    </Modal.Body>
                    <Modal.Footer>
                    </Modal.Footer>
                </Modal>
                <div className = "row" style={{paddingTop:"10px"}}>
                    <div className= "col-md-10 col-sm-10 col-xs-10" style={{textAlign:"left"}}>
                        <label className="qa-header" style={{fontWeight:"400",fontFamily:"calibri",fontSize:"20px",color:"grey"}}>MAIL DETAILS</label>
                    </div>
                    <div className= "col-md-2 col-sm-2 col-xs-2">
                        <span className="glyphicon glyphicon-pencil" onClick={()=>{this.setState({showModal:false})}}/>
                    </div>
                </div>
                <div style={{height:"1.5px",background:"lightgrey",width:"100%"}}/>
                <div className ="row" style={{paddingTop:"10px",paddingBottom:"20px"}}>
                    {
                        info.map((item,i)=>{
                            return (
                                <div className ="col-md-6">
                                    <FieldCell param={"Letter 1 Date "} value={item.mail_by_date}/>
                                    <FieldCell param={"Letter 1 Personal Text "} value={item.body_text}/>
                                    <FieldCell param={"Letter 1 PS Text "} value={item.ps_text}/>
                                    <FieldCell param={"Mailing id"} value={item.mailing_id}/>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}

module.exports = Mail
