import React, { Component } from "react";
import FieldCell from "../common/FieldCell"
import { updateProject, UPDATE_PROJECT } from "../../actions";
import store from "../../store";
import { Grid, Row, Col, MenuItem, Button, Modal } from "react-bootstrap";


class ModalView extends Component {
    constructor(props){
        super(props)
        this.state = {
            modified_enrollment: "",
            modified_registration_type : "",
            modified_imageshare_upload_allowed: false,
            modified_imageshare_upload_code:""
        }
        this.translateRegistrationType = this.translateRegistrationType.bind(this)
        this.handleEnroll = this.handleEnroll.bind(this)
    }

    componentWillMount(){
        this.setState({
            modified_enrollment: this.props.modalInfo.enrollment.value,
            modified_registration_type : this.props.modalInfo.registrationType.value,
            modified_imageshare_upload_allowed: this.props.modalInfo.imageshareUploadAllowed.value === "Enabled",
            modified_imageshare_upload_code:this.props.modalInfo.imageUploadCode.value
        })
    }

    handleEnroll(NewEnroll){
        if(!isNaN(parseFloat(NewEnroll)) && isFinite(NewEnroll) && parseFloat(NewEnroll) >= 0) {
            this.setState({modified_enrollment: NewEnroll});
        }
    }

    translateRegistrationType(project_registration_type_id){
        var regName = "";
        this.props.regTypes.map((regType,i)=> {
            if (regType.id === project_registration_type_id) {
            regName = regType.registration_type;
        }
    });
        return regName;
    }

    componentWillReceiveProps(newProps){
        this.setState({
            modified_enrollment: newProps.modalInfo.enrollment.value,
            modified_registration_type : newProps.modalInfo.registrationType.value,
            modified_imageshare_upload_allowed: newProps.modalInfo.imageshareUploadAllowed.value === "Enabled",
            modified_imageshare_upload_code:newProps.modalInfo.imageUploadCode.value
        })
    }

    render() {
        return(
            <div>
            <Modal id="qa-project-modal" show={this.props.showModal}>
                <div style={{textAlign:"center"}}>
                    <div className = "row center" style={{padding:"10px"}}>
                        <div id="project-overview-projectId" className= "col-md-10 col-sm-10 col-xs-10" style={{textAlign:"left"}}>
                            <label style={{fontWeight:"400",fontFamily:"calibri",fontSize:"18px",color:"grey"}}>PROJECT</label>
                        </div>
                        <div id= "project-overview-dismiss" className= "col-md-2 col-sm-2 col-xs-2">
                            <span className="glyphicon glyphicon-remove-sign"  style= {{fontSize:20}} onClick={()=>{this.props.dismissModal()}}/>
                        </div>
                    </div>
                    <div style={{height:"1.5px",marginLeft:"10px",marginRight:"10px"}}/>
                        <div className ="row" style={{marginTop:"10px"}}>
                            <ModelCell lValue = { {label: this.props.modalInfo.projectId.key, disable:false} } rValue = { {label: this.props.modalInfo.projectId.value, disable:true} }/>
                            <ModelCell lValue = { {label: this.props.modalInfo.projectName.key, disable:false} } rValue = { {label: this.props.modalInfo.projectName.value, disable:true} }/>
                            <ModelCell lValue = { {label: this.props.modalInfo.year.key, disable:false} } rValue = { {label: this.props.modalInfo.year.value, disable:true} }/>
                                <div style={{paddingTop:"20px",paddingBottom:"20px"}}>
                                    <div className="col-md-4" style={{textAlign:"right"}}>
                                        <label style={{fontWeight:"400"}}>Enrollment</label>
                                </div>
                                <div id="project-overview-enrollment" className="col-md-7" style={{textAlign:"left",color:"black", }}>
                                    <input style={{border: "1px solid black"}} value={this.state.modified_enrollment} onChange = {(e)=>{this.handleEnroll(e.target.value)}}/>
                            </div>
                            <div className="col-md-1"></div>
                        </div>
                            <ModelCell lValue = { {label: this.props.modalInfo.deliverySeason.key, disable:false} } rValue = { {label: this.props.modalInfo.deliverySeason.value, disable:true} }/>
                            <ModelCell lValue = { {label: this.props.modalInfo.salesProgram.key, disable:false} } rValue = { {label: this.props.modalInfo.salesProgram.value, disable:true} }/>
                            <ModelCell lValue = { {label: this.props.modalInfo.salesPackage.value, disable:false} } rValue = { {label: this.props.modalInfo.salesPackage.key, disable:true} }/>
                                <div style={{paddingTop:"20px",paddingBottom:"20px"}}>
                                    <div className="col-md-4" style={{textAlign:"right"}}>
                                        <label style={{fontWeight:"400"}}>Registration Type :</label>
                                </div>
                                <div id="project-overview-registration" className="col-md-7" style={{textAlign:"left",color:"black"}}>
                                    <select className="" onChange={(e)=>{this.setState({modified_registration_type:e.target.value})}} value={this.state.modified_registration_type}>
                {
                    this.props.regTypes.map((eachType,i)=>{
                        return  <option key={i} value={eachType.registration_type}>{ eachType.registration_type }</option>
                })
            }
            </select>
        </div>
            <div className="col-md-1"></div>
            </div>
                <div style={{paddingTop:"20px",paddingBottom:"20px"}}>
                    <div className="col-md-4" style={{textAlign:"right"}}>
                        <label style={{fontWeight:"400"}}>Imageshare Uploads :</label>
                </div>
                    <div className="col-md-7" style={{textAlign:"left",color:"black"}}>
                <div className="row">
                    <div id="project-overview-imageShare" className = "col-md-12">
                        <input type="radio" checked={this.state.modified_imageshare_upload_allowed} onChange= {(e)=>{this.setState({modified_imageshare_upload_allowed: true})}}/>
                            <label style={{fontWeight:"400"}}>Enabled</label>
                                <input type="radio" checked={!this.state.modified_imageshare_upload_allowed} onChange= {(e)=>{this.setState({modified_imageshare_upload_allowed:false})}} style={{marginLeft:"20px"}}/>
                                    <label style={{fontWeight:"400"}}>Disabled</label>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-1">
                </div>
            </div>
                <div style={{paddingTop:"20px",paddingBottom:"20px"}}>
                    <div className="col-md-4" style={{textAlign:"right"}}>
                        <label style={{fontWeight:"400"}}>Upload Code<label style={{fontWeight:"200"}}>(optional)</label></label>
                </div>
                    <div id="project-overview-upload" className="col-md-7" style={{textAlign:"left",color:"black"}}>
                <input  style={{border: "1px solid black"}} value={this.state.modified_imageshare_upload_code} onChange = {(e)=>{this.setState({modified_imageshare_upload_code:e.target.value})}}/>
            </div>
                    <div className="col-md-1">
                </div>
            </div>
                <ModelCell lValue = { {label: this.props.modalInfo.exactQuantityDate.key, disable:false} } rValue = { {label: this.props.modalInfo.exactQuantityDate.value, disable:true} }/>
                <ModelCell lValue = { {label: this.props.modalInfo.personalaizationDate.key, disable: false} } rValue = { {label: this.props.modalInfo.personalaizationDate.value, disable:true} }/>
            </div>
        </div>
            <Modal.Footer>
                <div className = "row">
                    <div className="col-md-10">
                </div>
                    <div id="project-oveview-save" className="col-md-2">
                        <Button onClick={()=>this.props.handleSave(this.state)}>Save</Button>
                    </div>
                </div>
            </Modal.Footer>
        </Modal>
        </div>
        )
    }
}

const ModelCell = (props) => {
    return (
        <div style={{paddingTop:"20px",paddingBottom:"10px"}}>
            <div className="col-md-4 col-sm-4 col-xs-4" style={{textAlign:"right",color: props.lValue.disable ? "lightgrey" : "black" }}>
                <label style={{fontWeight:"400"}}>{props.lValue.label} : </label>
            </div>
            <div className="col-md-8 col-sm-8 col-xs-8" style={{textAlign:"left",color: props.rValue.disable ? "lightgrey" : "black" }}>
                <label style={{fontWeight:"400"}}>{props.rValue.label}</label>
            </div>
                <div className="col-md-1"></div>
        </div>
    )
}

class ProjectDetail extends Component {
    constructor(props){
        super(props)

        this.translateRegistrationType = this.translateRegistrationType.bind(this);
        this.translateRegistrationId = this.translateRegistrationId.bind(this);
        this.dismissModal = this.dismissModal.bind(this)
        this.handleSave  = this.handleSave.bind(this);
        this.state = {
            projectInfo : {},
            showModal: false,
            enrollment: "",
            registration_type: "",
            id: "",
            imageshare_upload_code: "",
            imageshare_upload_allowed : "",
        }
    }

    componentWillMount(){
        this.setState({
            projectInfo : this.formateData(this.props.info)
        })
    }

    formateData(props){
        var projectInfo =   {
            projectName: { value: props.project_name, key: "Project Name"},
            projectId: { key:"Project#", value: props.project_number},
            year: { value:props.year, key:"Year" },
            enrollment:{ value:props.enrollment, key:"Enrollment"},
            deliverySeason : { value: props.delivery_season, key:"Delivery Season"},
            salesProgram : { value:props.sales_program,key:"Sales Program"},
            salesPackage : {value:props.sales_package, key:"Sales Package"},
            registrationType : {value:this.translateRegistrationType(props.project_registration_type_id),key:"Registration Type"},
            imageshareUploadAllowed: {value:props.imageshare_upload_allowed,key:"ImageShare Uploads"},
            imageUploadCode:{value:props.imageshare_upload_code,key:"Upload Code"},
            exactQuantityDate:{value:props.exact_quantity_date || "",key:"Exact Quantity Code"},
            personalaizationDate:{value:props.personalization_date || "",key:"Personalization Date"}
        }
        return projectInfo
    }

    handleSave(modified_data){
            JSON.stringify({
                "enrollment": modified_data.modified_enrollment,
                "imageshare_upload_code" : modified_data.modified_imageshare_upload_code,
                "imageshare_upload_allowed" : modified_data.modified_imageshare_upload_allowed? "Enabled" : "Disabled",
                "project_registration_type_id": this.translateRegistrationId(modified_data.modified_registration_type)
            })

        fetch("/sbapi/projects/project", {
                method: "PATCH",
                headers: {"Content-type": "application/json"},
                credentials: "include",
                body: JSON.stringify({
                    "enrollment": modified_data.modified_enrollment,
                    "imageshare_upload_code" : modified_data.modified_imageshare_upload_code,
                    "imageshare_upload_allowed" : modified_data.modified_imageshare_upload_allowed? "Enabled" : "Disabled",
                    "project_registration_type_id": this.translateRegistrationId(modified_data.modified_registration_type)
                })
            }
        ).then(function(response){
            return response.json();
        }).then((data)=>{
            this.setState({
            projectInfo: this.formateData(data.project),
        });
    }).catch((error)=> {
            console.log("Error Updating")
    })

        this.setState({
            showModal: false
        });
    }

    translateRegistrationId(regName){
        var regId = ""
        this.props.regTypes.map((regType,i)=> {
            if (regType.registration_type === regName) {
            regId = regType.id;
        }
    });
        return regId;
    }

    translateRegistrationType(project_registration_type_id){
        var regName = ""
        this.props.regTypes.map((regType,i)=> {
            if (regType.id === project_registration_type_id) {
            regName = regType.registration_type;
        }
    });
        return regName;
    }

    dismissModal(){
        this.setState({
            showModal:false
        })
    }

    render() {
        var projectInfo = []
        for(var key in this.state.projectInfo){
            projectInfo.push(this.state.projectInfo[key])
        }
        return (
            <div id="project-overview-main-panel" className="container-fluid" style={{background:"white"}}>
                <ModalView showModal={this.state.showModal} modalInfo = {this.state.projectInfo} regTypes = {this.props.regTypes} handleSave = {this.handleSave} dismissModal = {this.dismissModal} />
                    <div id="project-overview-panel" className = "row" style={{paddingTop:"10px"}}>
                        <div id="project-panel" className= "col-md-10 col-sm-10 col-xs-10" style={{textAlign:"left"}}>
                            <label className="qa-header" style={{fontWeight:"400",fontFamily:"calibri",fontSize:"18px",color:"grey"}}>PROJECT</label>
                    </div>
                        <div id="project-overview-pencil" className= "col-md-2 col-sm-2 col-xs-2">
                            <span className="glyphicon glyphicon-pencil" onClick={()=>{this.setState({showModal:true})}}/>
                    </div>
                </div>
                    <div style={{height:"1.5px",background:"lightgrey",width:"100%"}}/>
                        <div style={{paddingTop:"10px",paddingBottom:"10px",oveflow:"hidden"}}>
                {
                    projectInfo.map(function (eachField) {
                        return <FieldCell param={eachField.key} value={eachField.value} key={eachField.key}/>
                    }.bind(this))
                }
            </div>
        </div>);
    }
}

module.exports = ProjectDetail