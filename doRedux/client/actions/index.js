export const UPDATE_PROJECT = 'UPDATE_PROJECT';
export const UPDATE_SCHOOL = 'UPDATE_SCHOOL';
export const UPDATE_CONTACTS = 'UPDATE_CONTACTS';
export const SETUP_PROJECT= 'SETUP_PROJECT';

export function updateProject(data) {
  return {
    type: UPDATE_PROJECT,
    data
  };
}

export function updateSchool(data) {
  return {
    type: UPDATE_SCHOOL,
    data
  };
}

export function setupProject(data) {
  return {
    type: SETUP_PROJECT,
    data
  };
}

export function updateContacts(data) {
    return {
        type: UPDATE_CONTACTS,
        data
    };
}
