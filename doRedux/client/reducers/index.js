import { combineReducers } from 'redux';
import { SETUP_PROJECT, UPDATE_CONTACTS, UPDATE_PROJECT, UPDATE_SCHOOL } from '../actions';

const initialState = {
    contacts:[],
    bookspec:{},
    booksales:{},
    deadlines: {},
    MainInfo:{},
    project : {},
    book:{},
    school:{},
    store:{},
    products:[]

}

function project(state = initialState, action) {
    /*  Async Calls Happen here and Data is fetched from rem
     ote server
     until server is up and running using a local json file to populate the data */

    switch(action.type) {
        case SETUP_PROJECT:
        {
            let newState = action.data;
            console.log("inside setup project reducer: ", action.data)
            return newState;
        }
        case UPDATE_PROJECT:
        {
            // Only partial state is represented by data.
            return Object.assign({}, state, {
                project: action.data
            })
        }
        case UPDATE_SCHOOL:
        {
            return Object.assign({}, state, {
                school: action.data
            })
        }
        case UPDATE_CONTACTS:
        {
            return Object.assign({}, state, {
                contacts: action.data
            })
        }
            break;
        default:
            return state;
    }

}

export default combineReducers({
    project
});
