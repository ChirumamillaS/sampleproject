import React, { Component } from 'react';
import { Grid, Row, Col, Modal } from 'react-bootstrap';
const ProjectDetail = require('../components/overview/ProjectDetail')
const SchoolDetail = require('../components/overview/SchoolDetail')
const KeyContacts = require('../components/overview/KeyContacts')
const Store = require('../components/overview/Store')
const BookSpec = require('../components/overview/BookSpec')
const Deadlines = require('../components/overview/Deadlines')
const BookSales = require('../components/overview/BookSales')
const Mail = require('../components/overview/Mail')


class OverviewHome extends Component {
    constructor(props){
        super(props);
        this.state = {
            showModal: false
        }

    }
    render() {
        return (
            <div className ='container-fluid'>
                <div className ='row'>
                    <div className='col-md-10 col-sm-10'>
                        <div className = 'col-md-12 col-sm-12' style={{background:'white',marginLeft:-15}}>
                            <div className = 'col-md-4 col-sm-4' style={{marginTop:'10px'}}>
                                <ProjectDetail info = { this.props.projectInfo.project} regTypes ={this.props.projectInfo.project_registration_types}/>
                                <BookSales info = { this.props.projectInfo.book_sales}/>
                            </div>
                            <div className = 'col-md-4 col-sm-4' style={{marginTop:'10px',marginLeft:-30}}>
                                <BookSpec info = { this.props.projectInfo.book_spec}/>
                                <Deadlines info = { this.props.projectInfo.deadlines} />
                            </div>
                            <div className = 'col-md-4 col-sm-4' style={{marginTop:'10px',marginLeft:-30}}>
                                <SchoolDetail info = { this.props.projectInfo.school} grades={ this.props.projectInfo.grades }/>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-2 col-sm-2' style={{marginTop:'10px',marginLeft:-30}}>
                        <KeyContacts is_csr={ this.props.projectInfo.is_csr } info = {this.props.projectInfo.contacts} csrInfo = {this.props.projectInfo.csrs}/>
                    </div>
                </div>
                <div style={{marginTop:'10px'}}>
                    <Store info = {this.props.projectInfo.store} products = {this.props.projectInfo.products} school={ this.props.projectInfo.school } />
                </div>
                <div style={{width:'80%',marginTop:'10px'}}>
                    <Mail info = {this.props.projectInfo.mail}/>
                </div>
            </div>
        )
    }
}

module.exports = OverviewHome
