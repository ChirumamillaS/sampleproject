import React, { Component } from 'react';
import { Grid, Row, Col, Modal } from 'react-bootstrap';
const ProjectDetail = require('../components/overview/ProjectDetail')
const SchoolDetail = require('../components/overview/SchoolDetail')
const KeyContacts = require('../components/overview/KeyContacts')
const Store = require('../components/overview/Store')
const BookSpec = require('../components/overview/BookSpec')
const Deadlines = require('../components/overview/Deadlines')
const BookSales = require('../components/overview/BookSales')
const Mail = require('../components/overview/Mail')


class StoreHome extends Component {

    render() {
        console.log("StoreHome Props",this.props)
       // return <div/>
        return (
            <div className ='container-fluid'>
                <div style={{marginTop:'10px'}}>
                    <Store info = {this.props.projectInfo.store} products = {this.props.projectInfo.products} school={ this.props.projectInfo.school } />
                </div>
            </div>
        )
    }
}

module.exports = StoreHome
