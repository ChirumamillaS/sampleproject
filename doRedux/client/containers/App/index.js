import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as AppActions from '../../actions';
import { Grid, Row, Col, Tab, Nav, NavItem } from 'react-bootstrap';
const OverviewHome = require('../OverviewHome');
const Dashboard = require('../Dashboard');
const ProjectHome = require('../ProjectHome');
const StoreHome = require('../StoreHome');
const FormHome = require('../FormHome');
const Header = require('../../components/common/Header');
const Tabs = require('../../components/common/Tabs');
import store from '../../store';
import { setupProject } from '../../actions';

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            loaded: false
        }
    }

//loads data from a remote endpoint
    componentDidMount(){
        var that = this;
        fetch('/sbapi/projects/overview', {credentials: 'same-origin'})
            .then(function(response){
                return response.json();
            })
            .then(function(data){
                console.log("Data Fetched is",data)
                let setup_project_action = setupProject(data);
                store.dispatch(setup_project_action);
                that.setState({
                    loaded: true
                })
            });
    }

    render(){
        if(!this.state.loaded){
            return (
                <div>
                    Loading . .
                </div>
            )
        }
        return (
            <div className="container-fluid">
                <div className="black-text-color">
                    <Tab.Container id="project-overview-tab" className="project-overview-tabs" defaultActiveKey="project">
                        <Row className="clearfix">
                            <Nav bsStyle="pills">
                                <NavItem eventKey="project">
                                    PROJECT
                                </NavItem>
                                <NavItem eventKey="store">
                                    STORE
                                </NavItem>
                                <NavItem eventKey="forms">
                                    FORMS
                                </NavItem>

                            </Nav>
                            <Tab.Content animation>
                                <Tab.Pane eventKey="project">
                                    <ProjectHome projectInfo = {this.props.project}/>
                                </Tab.Pane>
                                <Tab.Pane eventKey="store">
                                    <StoreHome projectInfo = {this.props.project}/>
                                </Tab.Pane>
                                <Tab.Pane eventKey="forms">
                                    <FormHome projectInfo = {this.props.project}/>
                                </Tab.Pane>
                            </Tab.Content>
                        </Row>
                    </Tab.Container>
                </div>
            </div>
        );
    }
}
//single argument of the entire Redux store’s state and returns an object to be passed as props.
function mapStateToProps(state) {
    return {
        project : state.project
        //Project Object is set to this component props
    };
}

function mapDispatchToProps(dispatch) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
