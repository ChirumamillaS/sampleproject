'use strict';

var path = require( 'path' );
var webpack = require( 'webpack' );

module.exports = {
    entry: './client/index.js',
    output: {
        filename: 'project-overview-web.js'
    },
    module: {
        loaders: [ {
            test: /\.js?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                "presets": [ "react", "es2015" ]
            }
        },
        {
            test: /\.less$/,
            exclude: /node_modules/,
            loader: 'style-loader!css-loader!less-loader'
        },
        {
            test: /\.css$/,
            exclude: /node_modules/,
            loader: "style-loader!css" } ]
    },
    externals: {
        // Use external version of React
        "react": "React",
        "react-dom": "ReactDOM"
    }
};
