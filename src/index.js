


import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import Projectactions from "./actions/projectactions";
import Projectstore from "./ProjectOverViewStore";
import ProjectOverView from "./Components/ProjectOverView";
import {createStore} from "redux";


  const Html = React.createClass(
    {
      componentWillMount()
      {
        Projectactions.getProjectInfo();
      },
      render: function()
      {
        return (
            <ProjectOverView/>
        );
      }
    }
  )


ReactDOM.render(
  <Provider store={Projectstore}>
<Html/>
  </Provider>
  ,document.getElementById('content'));
