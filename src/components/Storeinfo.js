

import React from "react";
import {connect} from "react-redux";

var Storeinfo = React.createClass(
  {
eachsubinfo : function (obj2,z){
  return
  (
     <div className="row xtra-child" key={z}>
    <div className="col-md-4">
                               {obj2.productname}
                              </div>
                              <div className="col-md-2">
                               {obj2.grade}
                              </div>
                              <div className="col-md-2">
                               {obj2.price}
                              </div>
                              <div className="col-md-2">
                                 {obj2.start}
                              </div>
                              <div className="col-md-2">
                                 {obj2.end}
                              </div>
    </div>
  )
},
eachproductinfo: function(obj,i) {
  return(
    <div key={i}>
       <div className="row row-data" >
       <div className="col-md-4">
                                  {obj.productname}
                                 </div>
                                 <div className="col-md-2">
                                  {obj.grade}
                                 </div>
                                 <div className="col-md-2">
                                  {obj.price}
                                 </div>
                                 <div className="col-md-2">
                                    {obj.start}
                                 </div>
                                 <div className="col-md-2">
                                    {obj.end}
                                 </div>
       </div>
       </div>
  )
},
eachproduct: function(ob,y) {
    return(

       <div className="full-row" key={y}>
       <div className="row">
                                    <div className="col-md-12 text-bold div-row">
      <i className="glyphicon glyphicon-triangle-bottom"></i>  { ob.productname }
      </div>
                               </div>
{ ob.ProductsInfo.map(this.eachproductinfo) }
       </div>


  );
},

render: function render ()
{
    return (
      <div className="row">
      <div className="col-md-12">
      <div className="panel ">
      <div className="panel-heading text-uppercase text-bold  border">
      STORE <i className="glyphicon glyphicon-pencil "></i> </div>
      <div className="panel-body">
      <table className="table">
      <tbody><tr>
      <td>Store Type: </td>
    <td> { this.props.store.storetype }</td>
    <td>
    Sales Tax: </td>
    <td> { this.props.store.salestax }</td>
    <td>Sales Start Date: </td>
    <td> { this.props.store.salesstartdate }</td>
    <td>
    Final Sales Date: </td>
    <td> { this.props.store.finalsaledate }</td></tr></tbody></table>
    <div className="row header-table">
                               <div className="col-md-4">
                                   Product Name
                               </div>
                               <div className="col-md-2">
                                   Grade
                               </div>
                               <div className="col-md-2">
                                   Price
                               </div>
                               <div className="col-md-2">
                                   Start
                               </div>
                               <div className="col-md-2">
                                   End
                               </div>
                           </div>
                           <div>
                           { this.props.products.map(this.eachproduct) }
                           </div>
    </div></div>
    </div></div>
  )
    }
});

/*send data to components by mapping props*/

function mapStateToProps (state)
{
  return {
    store:state.store.store,
      products : state.store.products
  }
}

export default connect(mapStateToProps)(Storeinfo);
