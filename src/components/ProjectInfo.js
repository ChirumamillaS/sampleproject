import React from "react";
import {connect} from "react-redux";

var ProjectInfo = React.createClass ({

getInitialState() {
    return {
        editMode: false,
        projectName: "ProjectName",
        deliverySeason: "winter",
        salesPrograme: "Graduate",
        registartionType: "first",
        imageShareUploads: "none",
        uploadCode: "blue",
        exactQuantityCode: "Whole",
        personlaizationDate: "02/10/2017"

     };
  },

 componentWillMount(){
   console.log("componentWillMount", this.props);
   console.log("componentWillMount",JSON.stringify(this.props));
   this.setState({
     projectName: this.props.info.projectname,
     deliverySeason: this.props.info.deliveryseason,
     salesPrograme: this.props.info.salesprogram,
     registartionType: this.props.info.registraiontype,
     imageShareUploads: this.props.info.imageshareuploads,
     uploadCode: this.props.info.uploadcode,
     exactQuantityCode: this.props.info.exactquantitycode,
     personlaizationDate: this.props.info.personalaization
   })
 },

 viewField: function(attribute,value,key){

 if(this.state.editMode){
    return(
        <tr>
           <td>{attribute}</td>
           <input type="text" style={{height:30, width:100}}value={value} onChange={(input)=>{this.setState({[key]:input.target.value},()=>{console.log("ChangedKey",key)})}}/>
        </tr>
    )
 }
    return(
        <tr>
           <td>{attribute}</td>
           <td>{value}</td>
        </tr>
    )
 },

render: function(){
    return(
      <div className="col-md-4">
            <div className="row">
                <div className="col-md-12">
                    <div className="panel ">
                        <div className="panel-heading text-uppercase text-bold ">
                            PROJECT
                            <button className = "editButton" onClick={()=>{this.setState({editMode:!this.state.editMode})}}>
                            {this.state.editMode ? <i className="glyphicon glyphicon-ok"></i>:<i className="glyphicon glyphicon-pencil"></i>}
                            </button>
                        </div>
                        <div className="panel-body">
                            <table className="table  table-responsive">
                                <tbody>
                                    {this.viewField("Project#",this.state.projectName,"projectName")}
                                    {this.viewField("Devlivery Season",this.state.deliverySeason,"deliverySeason")}
                                    {this.viewField("Sales Program",this.state.salesPrograme,"salesPrograme")}
                                    {this.viewField("Registartion type :",this.state.registartionType,"registartionType")}
                                    {this.viewField("imageShare Uploads",this.state.imageShareUploads,"imageShareUploads")}
                                    {this.viewField("Upload Code",this.state.uploadCode,"uploadCode")}
                                    {this.viewField("Exact Quantity Code",this.state.exactQuantityCode,"exactQuantityCode")}
                                    {this.viewField("Personlaization Date",this.state.personlaizationDate,"personlaizationDate")}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="panel ">
                            <div className="panel-heading text-uppercase text-bold  border">
                                Book Specification
                                <i className="glyphicon glyphicon-pencil "></i>
                            </div>
                            <div className="panel-body">
                                <table className="table  table-responsive">
                                  <tbody><tr>
                                        <td>Pages/Copies :</td>
                                        <td>{this.props.book.pages}
                                        /{this.props.book.copies}</td>
                                    </tr>
                                    <tr>
                                        <td>Trim Size :</td>
                                        <td>{this.props.book.TrimSize}</td>
                                    </tr>
                                    <tr>
                                        <td>Cover Type :</td>
                                        <td>{this.props.book.covertype}</td>
                                    </tr>
                                    <tr>
                                        <td>Personalization Type :</td>
                                        <td>{this.props.book.personalizationtype}</td>
                                    </tr>
                                    <tr>
                                        <td>Requested Ship Date :</td>
                                        <td>{this.props.book.requstedshipdate}</td>
                                    </tr>
                                    <tr>
                                        <td>Yearbook Software :</td>
                                        <td>{this.props.book.YearbookSoftware}</td>
                                    </tr></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    );
}
});

/*send data to components by mapping props*/

function mapStateToProps (state)
{
  return {
      project : state.store.project,
      book:state.store.book
  };
}

export default connect(mapStateToProps)(ProjectInfo);
