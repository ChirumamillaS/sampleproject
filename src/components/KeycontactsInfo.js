

import React from 'react';
import {connect} from "react-redux";


var KeycontactsInfo = React.createClass ({
eachcontact : function(obj,i)
{
    return (
         <div className="list-group-item" key={i}>
                            <table className="table person-info">
                                 <tbody><tr>
                                    <td>
                                        <img src={obj.img} className="img-circle" />
                                    </td>
                                    <td>
                                        <p><strong>{obj.name}</strong></p>
                                        <p>{obj.desg}</p>
                                        <p><a href={"mailto:"+obj.email}>{obj.email}</a></p>
                                        <p>{obj.phone}</p>
                                    </td>
                                </tr></tbody>
                            </table>
                        </div>
        );
},
render: function render()
{

    return(
      <div className="col-md-4">
            <div className="panel">
                <div className="panel-heading text-uppercase text-bold ">
                    KEY CONTACTS
                    <i className="glyphicon glyphicon-pencil "></i>
                </div>
                <div className="panel-body">
                    <div className="list-group-unbordered">
                       {this.props.contacts.map(this.eachcontact)}
                    </div>
                </div>
            </div>
        </div>
);

}

});

/*send data to components by mapping props*/

function mapStateToProps (state)
{
  return {
      contacts : state.store.contacts
  }
}


export default connect(mapStateToProps)(KeycontactsInfo);
