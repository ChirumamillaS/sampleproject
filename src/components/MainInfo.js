
 import React from "react";
import {connect} from "react-redux";

    var MainInfo = React.createClass (
      {
          render: function render ()
          {
              return (
                <div className="row">
                <div className="col-md-12">
                <div className="panel ">
                <div className="panel-heading text-uppercase text-bold  border">
                MAIL DETAILS <i className="glyphicon glyphicon-pencil "></i></div>
                <div className="panel-body">
                <table className="table tbl-mail">
                <tbody><tr>
                <td>  Letter 1 Date: </td>
              <td> { this.props.MainInfo.letter1date }</td>
            <td>Letter 2 Date: </td>
              <td> { this.props.MainInfo.letter2date }</td></tr>
              <tr><td>Letter 1 PersonalText: </td>
              <td> { this.props.MainInfo.letter1personaltext }</td>
              <td>Letter 2 PersonalText: </td>
              <td> { this.props.MainInfo.letter2personaltext }</td></tr>
              <tr><td>Letter 1 PS TEXT: </td>
              <td>{ this.props.MainInfo.letter1pstext }</td>
              <td>Letter 2 PS TEXT:</td>
              <td>{ this.props.MainInfo.letter2pstext }</td></tr></tbody></table>
              </div></div>
              </div></div>
                );
        }
        });

        /*send data to components by mapping props*/

        function mapStateToProps (state)
        {
          return {
              MainInfo :state.store.MainInfo
          }
        }

        export default connect(mapStateToProps)(MainInfo);
