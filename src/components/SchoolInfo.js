


import React from "react";
import {connect} from "react-redux";
import InlineEdit from 'react-edit-inline';

var SchoolInfo = React.createClass ({

render: function render()
{
    return(
        <div className="col-md-4">

            <div className="panel ">
                <div className="panel-heading text-uppercase text-bold ">
                    SCHOOL
                    <i className="glyphicon glyphicon-pencil "></i>
                </div>
                <div className="panel-body">
                    <table className="table  table-responsive">
                       <tbody><tr>
                            <td>School Name :</td>
                            <InlineEdit
                                  activeClassName="editing"
                                  text={this.props.info.schoolname}
                                  paramName="message"
                                  style={{
                                    backgroundColor: 'white',
                                    display: 'inline-block',
                                    margin: 0,
                                    padding: 0,
                                    fontSize: 15,
                                    outline: 0,
                                    border: 0
                                  }}
                                />
                        </tr>
                        <tr>
                            <td>Year :</td>
                            <td>{this.props.school.Year}</td>
                        </tr>
                        <tr>
                            <td>Grades :</td>
                            <td>{this.props.school.grades}</td>
                        </tr>
                        <tr>
                            <td>Enrollment :</td>
                            <td>{this.props.school.enrollement}</td>
                        </tr>
                        <tr>
                            <td>Tax Exempt :</td>
                            <td>{this.props.school.taxExempt}</td>
                        </tr>
                        <tr>
                            <td>Billing Address :</td>
                            <td><p>{this.props.school.billingaddressaddress1}</p>  <p>{this.props.school.billingaddressaddress2}</p>
                            <p>{this.props.school.billingaddressaddress3}</p></td>
                        </tr>
                        <tr>
                            <td>Shipping Address :</td>
                            <td><p>{this.props.school.shippingadressaddress1}</p>  <p>{this.props.school.shippingadressaddress2}</p>
                            {this.props.school.shippingadressaddress3}</td>
                        </tr>
                        <tr>
                            <td>School Phone :</td>
                            <td>{this.props.school.schoolphone}</td>
                        </tr>
                        <tr>
                            <td>Adviser :</td>
                            <td><p>{this.props.school.advisername}</p>  <p>{this.props.school.advisermail}</p></td>
                        </tr>
                        <tr>
                            <td>Sales Region:</td>
                            <td>{this.props.school.salesregion}</td>
                        </tr>
                        <tr>
                            <td>Sales Office :</td>
                            <td>{this.props.school.salesoffice}</td>
                        </tr></tbody>
                    </table>
                </div>
            </div>
        </div>
);

}

});

/*send data to components by mapping props*/

function mapStateToProps (state)
{
  return {
      school : state.store.school
  }
}

export default connect(mapStateToProps)(SchoolInfo);
