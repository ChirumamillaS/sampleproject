import React from 'react';
import Menuhtml from "./Menuhtml";
import ProjectInfo from "./ProjectInfo";
import KeycontactsInfo from "./KeycontactsInfo";
import Storeinfo from "./Storeinfo";
import MainInfo from "./MainInfo";
import SchoolInfo from "./SchoolInfo";
import Projectstore from "../actions/projectactions"

var Data = require('./../../data.json')
console.log("Data",Data);
var ProjectOverView = React.createClass ({
    render : function(){
        return(
            <div>
              <Menuhtml/>
              <div className='row'>
                  <ProjectInfo info = {Data.Project} book = {Data.Book}/>
                  <SchoolInfo info = {Data.School}/>
                  <KeycontactsInfo />
              </div>
              <Storeinfo/>
              <MainInfo/>
            </div>
            );
    }
}
);

 export default ProjectOverView;
