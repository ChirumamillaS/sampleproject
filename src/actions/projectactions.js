
import ProjectConstatnts from "./ProjectConstants";
import ProjectAPI from "../projectapi/api";
import Projectstore from "../ProjectOverViewStore"

const intilizeProjectInfo =  function()
{
  ProjectAPI.getProjectDetails()
              .done((data)=>
              {
                let response = JSON.parse(data);
                Projectstore.dispatch(
                {
                  type:ProjectConstatnts.loadProjectInfo,
                  payload:response
                }
              )
              }

            )
};

const ProjectActions = {

  getProjectInfo : function ()
  {
      return intilizeProjectInfo();
  }

}


export default ProjectActions;
