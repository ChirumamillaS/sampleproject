import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import ProjectOverView from "./components/ProjectOverView";
import Projectstore from "./ProjectOverViewStore";

ReactDOM.render(
  <Provider store= {Projectstore}>
  <ProjectOverView />
  </Provider> , document.getElementById('content'));
