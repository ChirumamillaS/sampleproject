import {createStore, combineReducers} from "redux";
import projectReducer from "./ProjectReducer";

const ComReduce = combineReducers (
  {
    store: projectReducer
  }
);

const Projectstore= createStore(ComReduce);

export default Projectstore;
