
import projectConstants from "./actions/ProjectConstants";


const projectInfo = {
  contacts:[],
  MainInfo:{},
  project : {},
  book:{},
  school:{},
  store:{},
  products:[]
}

const intilizeProjectOverview = function(state,payload){
  return  Object.assign({},state,{
    contacts:payload.Contacts,
    MainInfo:payload.MainInfo,
    project : payload.Project,
    book:payload.Book,
    school:payload.School,
    store:payload.Store,
    products:payload.Products
  });

}


function projectOverViewReducer (state=projectInfo,action)
{
  switch (action.type) {
    case projectConstants.loadProjectInfo:
      {
          let newState =intilizeProjectOverview(state,action.payload);
          return newState;
      }
      break;
    default:
    return state;
    break;
  }
}



export default projectOverViewReducer;
