'use strict';

var path = require( 'path' );
var webpack = require( 'webpack' );
// const sourcePath = path.join(__dirname, './client');
// const staticsPath = path.join(__dirname, './static');

module.exports = {
    entry: './index.js',
    output: {
        filename: 'store-setup-web.js'
    },
    module: {
        loaders: [ {
            test: /\.js?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                "presets": [ "react", "es2015" ]
            }
        },
            {
                test: /\.less$/,
                exclude: /node_modules/,
                loader: 'style-loader!css-loader!less-loader'
            },

            { test: /\.css$/, loader: "style!css" } ]
    },
    externals: {
        // Use external version of React
        "react": "React",
        "react-dom": "ReactDOM"
    },
};
