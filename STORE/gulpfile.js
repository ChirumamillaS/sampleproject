var gulp = require('gulp');
var webpack = require('gulp-webpack');
var watch = require('gulp-watch');
var less = require('gulp-less');

gulp.task('watch', function() {
	gulp.watch('actions/**/*.js', ['default']);
	gulp.watch('components/**/*.js', ['default']);
	gulp.watch('containers/**/*.js', ['default']);
	gulp.watch('reducers/**/*.js', ['default']);
	gulp.watch('./*.js', ['default']);
});

gulp.task('js', function() {
	return gulp.src('./index.js')
	  	.pipe(webpack( require('./webpack.config.js') ))
	  	.pipe(gulp.dest('../js'));
});


gulp.task('css', function() {
   	return gulp.src('./css/store-setup-web.css')
  		.pipe(gulp.dest('../css'))
});

gulp.task('default', function() {
  gulp.start('js', 'css');
})
