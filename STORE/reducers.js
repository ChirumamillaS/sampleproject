import { combineReducers } from 'redux';
import { app } from './reducers/AppReducers.js';
import { alert } from './reducers/AlertReducers.js';
import { program } from './reducers/ProgramReducers.js';
import { general } from './reducers/GeneralReducers.js';
import { product_modal } from './reducers/ProductModalReducers.js';
import { STORE_DATA_ALL } from './actions/AppActions.js';
import { UPDATE_PROGRAM_TYPE, UPDATE_PROGRAM_FEE } from './actions/ProgramActions.js';
import { UPDATE_OVERRIDE_TAX, UPDATE_PRICE_INCLUDES_TAX, UPDATE_TAX_OVERRIDE_AMOUNT } from './actions/GeneralActions.js';
import { GENERIC_STORE_UPDATE } from './actions/AppActions.js';

const initialState = {
                          "store_type_id": 1,
                          "store_status_id": 1,
                          "accessories_end_date": "",
                          "ads_end_date": "",
                          "collect_sales_tax": 0,
                          "confirmation": 0,
                          "confirmation_date": null,
                          "confirmation_name": null,
                          "default_sales_tax_rate": 0.0,
                          "exact_quantity_date": "",
                          "enrollment" : 0,
                          "shipping_fee" : 0,
                          "store_shipping_id" : 1,
                          "oncampus_sales_end_date": "",
                          "online_sales_end_date": "",
                          "override_sales_tax": 0,
                          "override_sales_tax_rate": 0.0,
                          "personalizations_end_date": "",
                          "store_program_fee_option_id": "1",
                          "project_id": 0,
                          "sales_close_date": "",
                          "sales_open_date": "",
                          "special_requests": null,
                          "store_id": 0,
                          "store_program_id": 0,
                          "store_type_id": 1,
                          "web_image_id": null,
                          "web_message": "",
                          "price_include_taxes": 0,
                          "school_grades": [],
                          "grades": [],
                          "program_fee_options": [],
                          "products": [],
}

function store_data(state: State = initialState, action: Action): State {
	switch(action.type) {
        case STORE_DATA_ALL:
            return Object.assign( {}, state, action.data );

		case UPDATE_OVERRIDE_TAX:
            let overrideAmount = {}
            if (!action.isOverrideTax){
                overrideAmount = { override_sales_tax_rate: '' };
            }

			return Object.assign(
				{}, state, {
					override_sales_tax: action.isOverrideTax,
                    },
                    overrideAmount
			);

        case UPDATE_TAX_OVERRIDE_AMOUNT:
            return Object.assign( {}, state, { override_sales_tax_rate: action.overrideValue });

		case UPDATE_PROGRAM_TYPE 	:
			return Object.assign(
				{}, state, {
					store_program_id: action.data,
                    store_program_fee_option_id: ''
				}
			);

        case UPDATE_PROGRAM_FEE:
            return Object.assign({}, state, { store_program_fee_option_id: action.programFeeId });

        case GENERIC_STORE_UPDATE:
            let update = {};
            update[ action.name ] = action.value
            return Object.assign( {}, state, update );

		default :
			return state;
		}
}

export default combineReducers({
    store_data,
    app,
    alert,
    program,
    product_modal,
    general
});
