const UPDATE_PROGRAM_TYPE	= 'UPDATE_PROGRAM_TYPE';
const UPDATE_PROGRAM_FEE    = 'UPDATE_PROGRAM_FEE';

function updateProgramType(data) {
    return {
        type: UPDATE_PROGRAM_TYPE,
        data
    };
}

function updateProgramFee(programFeeId){
    return {
            type: UPDATE_PROGRAM_FEE,
            programFeeId
    };
}

module.exports = {
    UPDATE_PROGRAM_TYPE,
    updateProgramType,

    UPDATE_PROGRAM_FEE,
    updateProgramFee
};
