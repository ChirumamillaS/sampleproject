const UPDATE_OVERRIDE_TAX = 'UPDATE_OVERRIDE_TAX'
const UPDATE_PRICE_INCLUDES_TAX = 'UPDATE_PRICE_INCLUDES_TAX'
const UPDATE_TAX_OVERRIDE_AMOUNT = 'UPDATE_TAX_OVERRIDE_AMOUNT'

function updateOverrideTax(isOverrideTax) {
    return {
        type: UPDATE_OVERRIDE_TAX,
        isOverrideTax
    };
}

function updatePriceIncludesTax(isIncluded) {
    return {
        type: UPDATE_PRICE_INCLUDES_TAX,
        isIncluded
    };
}

function updateTaxOverrideAmount(overrideValue) {
    return {
        type: UPDATE_TAX_OVERRIDE_AMOUNT,
        overrideValue
    };
}

module.exports = {
    UPDATE_OVERRIDE_TAX,
    updateOverrideTax,

    UPDATE_PRICE_INCLUDES_TAX,
    updatePriceIncludesTax,

    UPDATE_TAX_OVERRIDE_AMOUNT,
    updateTaxOverrideAmount,
};
