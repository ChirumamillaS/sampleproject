const GENERIC_PRODUCT_MODAL_UPDATE  = "GENERIC_PRODUCT_MODAL_UPDATE";
const UPDATE_PRICE_TIER = "UPDATE_PRICE_TIER";
const UPDATE_PRODUCT_ALL = "UPDATE_PRODUCT_ALL";

function genericProductModalUpdate( name, value ) {
    return {
        type: GENERIC_PRODUCT_MODAL_UPDATE,
        name,
        value
    };
}

function updatePriceTier( tier_num, tier_data) {
    return {
        type: UPDATE_PRICE_TIER,
        tier_num,
        tier_data
    };
}

function updateProductAll( data ) {
    return {
        type: UPDATE_PRODUCT_ALL,
        data
    };
}

module.exports = {
    GENERIC_PRODUCT_MODAL_UPDATE,
    genericProductModalUpdate,

    UPDATE_PRICE_TIER,
    updatePriceTier,

    UPDATE_PRODUCT_ALL,
    updateProductAll
}



