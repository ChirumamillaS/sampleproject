const ALERT_VISIBLE	= 'ALERT_VISIBLE';
const ALERT_TEXT	= 'ALERT_TEXT';
const ALERT_TYPE	= 'ALERT_TYPE';

function isAlertVisible(data) {
    return {
        type: ALERT_VISIBLE,
        data
    };
}

function setAlertText(data) {
    return {
        type: ALERT_TEXT,
        data
    };
}

function setAlertType(data) {
    return {
        type: ALERT_TYPE,
        data
    };
}

module.exports = {
    ALERT_VISIBLE,
    isAlertVisible,

    ALERT_TEXT,
    setAlertText,

    ALERT_TYPE,
    setAlertType
};
