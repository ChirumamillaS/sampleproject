const STORE_DATA_ALL    	= 'STORE_DATA_ALL';
const SELECTED_TAB	        = 'SELECTED_TAB';
const UPDATE_DEFAULTS       = 'UPDATE_DEFAULTS';
const STORE_STATUS	        = 'STORE_STATUS';
const ENABLE_GENERAL_TAB	= 'ENABLE_GENERAL_TAB';
const ENABLE_PRODUCT_TAB	= 'ENABLE_PRODUCT_TAB';
const GENERIC_STORE_UPDATE  = 'GENERIC_STORE_UPDATE';

function updateStoreDataAll( data ) {
    return {
        type: STORE_DATA_ALL,
        data
    };
}

function updateDefaults(data) {
    return {
        type: UPDATE_DEFAULTS,
        data
    };
}

function updateSelectedTab(data) {
    return {
        type: SELECTED_TAB,
        data
    };
}

function getStoreStatus( data ) {
    return {
        type: STORE_STATUS,
        data
    };
}

function enableGeneralTab( data ) {
    return {
        type: ENABLE_GENERAL_TAB,
        data
    };
}

function enableProductTab( data ) {
    return {
        type: ENABLE_PRODUCT_TAB,
        data
    };
}

function genericStoreUpdate( name, value ) {
    return {
        type: GENERIC_STORE_UPDATE,
        name,
        value
    };
}

module.exports = {
    STORE_DATA_ALL,
    updateStoreDataAll,

    SELECTED_TAB,
    updateSelectedTab,

    UPDATE_DEFAULTS,
    updateDefaults,

    STORE_STATUS,
    getStoreStatus,

    ENABLE_GENERAL_TAB,
    enableGeneralTab,

    ENABLE_PRODUCT_TAB,
    enableProductTab,

    GENERIC_STORE_UPDATE,
    genericStoreUpdate
};
