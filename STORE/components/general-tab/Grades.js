import React, { Component } from "react";
import { Grid, Row, Col, Modal } from "react-bootstrap";
import "../../../components-web/multi-select/multi-select.less";
import MultiSelectKeyValue from "../../../components-web/multi-select/multiselect_keyvalue.js";


class Grades extends Component {
    render() {
        return (
            <div>
                <MultiSelectKeyValue items={ this.props.availableGrades } key_name={ "id" } value_name={ "grade" } selected={ this.props.selectedGrades } onChange={ ( e )=>{ this.props.handleStoreGradeChange( e ) } } />
            </div>
        );
    }
}

module.exports = Grades;
