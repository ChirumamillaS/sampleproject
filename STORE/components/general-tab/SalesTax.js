import React, { Component } from 'react';
import { Row, Col, FormControl, ControlLabel, Radio, Panel } from 'react-bootstrap';
import accounting from 'accounting';
import store from '../../store.js';

// actions
import { updateOverrideTax, updateTaxOverrideAmount } from '../../actions/GeneralActions.js';
import { genericStoreUpdate } from "../../actions/AppActions.js";

class SalesTax extends Component {
    constructor(props) {
        super(props);

        this.handleOverrideTaxUI = this.handleOverrideTaxUI.bind(this);
        this.handleOverrideTax = this.handleOverrideTax.bind(this);
        this.handlePriceIncludesTax = this.handlePriceIncludesTax.bind(this);
        this.handleOverrideTaxValue = this.handleOverrideTaxValue.bind(this);
        this.formatOverrideTaxValue = this.formatOverrideTaxValue.bind(this);
    }

    handleOverrideTaxUI() {
        if ( this.props.override_sales_tax ) {
            return (
                <Col className="text-align-left" componentClass={ ControlLabel } md={ 8 }>
                    <Col componentClass={ ControlLabel } md={ 4 }>
                        <Radio inline value={ 1 } name="overrideTax" onChange={ this.handleOverrideTax } checked={ this.props.override_sales_tax === 1 } > Yes </Radio>
                    </Col>
                    <Col componentClass={ ControlLabel } md={ 4 }>
                        <FormControl className="override-tax" type="text" value={ this.props.override_sales_tax_rate } onChange={ this.handleOverrideTaxValue } onBlur={ this.formatOverrideTaxValue } />
                    </Col>
                </Col>
            );
        }

        return (
            <Col className="text-align-left" componentClass={ ControlLabel } md={ 8 }>
                <Radio inline value={ 1 } name="overrideTax" onChange={ this.handleOverrideTax } checked={ this.props.override_sales_tax === 1 } > Yes </Radio>
            </Col>
        );
    }

    handleOverrideTax(e) {
        const val = parseInt(e.target.value);
        store.dispatch(updateOverrideTax(val));
    }

    handlePriceIncludesTax(e) {
        let val = (parseInt(e.target.value));
        if ( [1, 2].includes( this.props.store_program_id ) ) {
            val = 1;
        }
        store.dispatch( genericStoreUpdate( "price_include_taxes", val ) );
    }

    formatOverrideTaxValue() {
        let value = accounting.formatNumber(this.props.override_sales_tax_rate, 3);
        if (0 > value || value > 15) {
            value = accounting.formatNumber(0.0, 3);
        }
        store.dispatch(updateTaxOverrideAmount(value + "%"));
    }

    handleOverrideTaxValue(e) {
        store.dispatch(updateTaxOverrideAmount(e.target.value));
    }

    render() {
        return (
            <Panel className="plain-panel-small">
                <Row>
                    <Col componentClass={ ControlLabel } md={ 4 }>
                        Tax Rate:
                    </Col>
                    <Col className="text-align-left" componentClass={ ControlLabel } md={ 8 }>
                        <span dangerouslySetInnerHTML={{ __html: this.props.default_sales_tax_rate }} ></span>%
                    </Col>
                </Row>
                <Row>
                    <Col componentClass={ ControlLabel } md={ 4 }>
                        Override Tax Rate:
                    </Col>
                    <Col className="text-align-left" componentClass={ ControlLabel } md={ 8 }>
                        <Col componentClass={ ControlLabel } md={ 4 }>
                            <Radio inline value={ 0 } name="overrideTax" onChange={ this.handleOverrideTax } checked={ this.props.override_sales_tax === 0 } > No </Radio>
                        </Col>
                        { this.handleOverrideTaxUI() } 
                    </Col>
                </Row>
                <Row>
                    <Col componentClass={ ControlLabel } md={ 4 }>
                        Include tax in Product Price:
                    </Col>
                    <Col componentClass={ ControlLabel } md={ 8 }>
                        <Col componentClass={ ControlLabel } md={ 4 }>
                            <Radio inline value={ 0 } name="price_include_taxes" onChange={ this.handlePriceIncludesTax } checked={ this.props.price_include_taxes === 0 } > No </Radio>
                        </Col>
                        <Col className="text-align-left" componentClass={ ControlLabel } md={ 8 }>
                            <Radio inline value={ 1 } name="price_include_taxes" onChange={ this.handlePriceIncludesTax } checked={ this.props.price_include_taxes === 1 } > Yes </Radio>
                        </Col>
                    </Col>
                </Row>
            </Panel>
        );
    }
}

module.exports = SalesTax;
