import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../store.js";
import { Grid, Row, Col, HelpBlock, FormGroup, ControlLabel, Panel } from "react-bootstrap";
import DatePicker from "react-bootstrap-date-picker";
import moment from "moment";

//actions
import { genericStoreUpdate } from "../../actions/AppActions.js";

class DeliverySeason extends Component {
    constructor( props ) {
        super( props );

        this.handleDateChange = this.handleDateChange.bind(this);
        this.toISOString = this.toISOString.bind(this);
    }

    handleDateChange( key, value ) {
        let date_part = value.split("T")[0];
        store.dispatch( genericStoreUpdate( key, date_part) );
    }

    toISOString( date_string ) {
        if ( date_string === "" ) {
            return "";
        }
        return new Date( date_string ).toISOString();
    }


    render() {
        let data = this.props.store_data;
        return (
            <FormGroup id="deliverySeason">
                <Col componentClass={ ControlLabel } md={ 2 }>
                    Delivery Season:
                </Col>
                <Col md={ 10 }>
                    <Row>
                        <Col md={ 1 }> <span>Fall</span> </Col>
                        <Col md={ 11 }>
                            <HelpBlock className="m0">
                                <em> (Delivery Season influencs your deadlines) </em>
                            </HelpBlock>
                        </Col>
                    </Row>
                    <Row>
                        <Panel className="plain-panel">
                            <Row>
                                <Col md={ 6 }>
                                    <Col componentClass={ ControlLabel } md={ 5 }> Sales Open: </Col>
                                        <Col md={ 7 }> <DatePicker value={ this.toISOString( data.sales_open_date ) } onChange={ ( value, formatted_value ) => {
                                            this.handleDateChange( "sales_open_date", value);
                                            }
                                        } />
                                    </Col>
                                </Col>
                                <Col md={ 6 }>
                                    <Col componentClass={ ControlLabel } md={ 5 }> Final Sales: </Col>
                                        <Col md={ 7 }> <DatePicker value={ this.toISOString( data.online_sales_end_date ) } onChange={ ( value, formatted_value ) => {
                                            this.handleDateChange( "online_sales_end_date", value );
                                        }
                                        } />
                                    </Col>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={ 6 }>
                                    <Col componentClass={ ControlLabel } md={ 5 }> On Campus Orders End: </Col>
                                        <Col md={ 7 }> <DatePicker value={ this.toISOString( data.oncampus_sales_end_date ) } onChange={ ( value, formatted_value ) => {
                                            this.handleDateChange( "oncampus_sales_end_date", value );
                                        }
                                        } />
                                    </Col>
                                </Col>
                                <Col md={ 6 }>
                                    <Col componentClass={ ControlLabel } md={ 5 }> Exact Quantity: </Col>
                                        <Col md={ 7 }> <DatePicker disabled={ true } />
                                    </Col>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={ 6 }>
                                    <Col componentClass={ ControlLabel } md={ 5 }> Personalizations End: </Col>
                                        <Col md={ 7 }> <DatePicker disabled={ true } />
                                    </Col>
                                </Col>
                                <Col md={ 6 }>
                                    <Col componentClass={ ControlLabel } md={ 5 }> Ads:  </Col>
                                        <Col md={ 7 }> <DatePicker value={ this.toISOString( data.ads_end_date ) } onChange={ ( value, formatted_value ) => {
                                            this.handleDateChange( "ads_end_date", value );
                                        }
                                        } />
                                    </Col>
                                </Col>
                            </Row>
                        </Panel>
                    </Row>
                </Col>
            </FormGroup>
        );
    }
}

function mapStateToProps(state) {
    return {
        store_data: state.store_data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DeliverySeason);
