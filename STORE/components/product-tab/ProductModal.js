import React, { Component } from "react";
import store from "../../store.js";
import { Grid, Row, Col, Modal, Button, ButtonToolbar } from "react-bootstrap";

//child components
import TopSection from "./product-modal/TopSection.js";
import Details from "./product-modal/Details.js";
import Personalizations from "./product-modal/Personalizations.js";
import Pricing from "./product-modal/Pricing.js";

// actions
import { genericProductModalUpdate, updateProductAll } from "../../actions/ProductActions.js";
import { updateStoreDataAll } from "../../actions/AppActions.js";

 // footer
import StoreSetupFooter from "../../components/StoreSetupFooter.js";

//api
import { makeAPIRequest } from "../../helper.js";

class ProductModal extends Component {
    constructor( props ){
        super( props );

        this.state = {
            showYearbookModal: false
        };

        this.showModal = this.showModal.bind( this );
        this.hideModal = this.hideModal.bind( this );
        this.handleSave = this.handleSave.bind( this );
    }

    showModal() {
        store.dispatch( genericProductModalUpdate( 'store_id', this.props.store_data.store_id ) );
        this.setState( { showModal: true } );
    }

    hideModal() {
        this.setState( { showModal: false } );
    }

    handleSave() {
        if( !this.props.product_modal.store_product_id ) {
            makeAPIRequest( "createproduct", this.props.store_data, this.props.product_modal );
        } else {
            //Make PATCH request to update yearbook..
            makeAPIRequest( "saveproduct", this.props.store_data, this.props.product_modal );
        }
    }

    render() {
        return (
            <ButtonToolbar>
                <span> Setup your yearbook first to manage your products. </span>
                    <Button bsStyle="link" onClick={ this.showModal }> Click here to continue. </Button>

                <Modal bsSize="large" show={ this.state.showModal } onHide={ this.hideModal } >
                    <Modal.Header closeButton>
                        <h2> ADD YEARBOOK </h2>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <TopSection />
                        </Row>
                        <Row>
                            <Col md={ 6 }>
                                <Details />
                            </Col>
                            <Col md={ 6 }>
                                <Personalizations />
                            </Col>
                        </Row>
                        <Row>
                            <Pricing />
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="store-setup-footer">
                            <StoreSetupFooter hideBack="true" hideConfirm="true" hideNext="true" onSaveClick={ this.handleSave } />
                        </div>
                    </Modal.Footer>
                </Modal>
            </ButtonToolbar>
        );
    }
}

module.exports = ProductModal;
