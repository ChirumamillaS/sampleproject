import React, { Component } from "react";
import { connect } from "react-redux";
import store from '../../../store.js';
import { Grid, Row, Col, Label, Modal, ControlLabel, FormControl, Table, FormGroup, InputGroup } from "react-bootstrap";
import { DatePickerWithId } from "./DatePickerWithId.js";
import accounting from "accounting";
import MultiSelectKeyValue from "../../../../components-web/multi-select/multiselect_keyvalue.js";
import { getGradesSubset } from "../../../helper.js";

// actions
import { updatePriceTier } from "../../../actions/ProductActions.js"

class Pricing extends Component {
    constructor( props ){
        super( props );

        if (this.props.store_data.collect_sales_tax ) {
            this.sales_tax_rate = ( this.props.store_data.override_sales_tax_rate || this.props.store_data.default_sales_tax_rate ) * 0.01;
        }
        else {
            this.sales_tax_rate = 0.00;
        }

        this.handleGenericPricingTierChange = this.handleGenericPricingTierChange.bind(this);
        this.handleGenericPricingTierChangeDate = this.handleGenericPricingTierChangeDate.bind(this);
        this.handlePricingTierGradeChange = this.handlePricingTierGradeChange.bind(this);
        this.handleChangeMoney = this.handleChangeMoney.bind(this);
        this.toISOString = this.toISOString.bind(this);
    }

    handleChangeMoney( e ) {
        const meta = e.target.id.split("|");
        const value = accounting.formatNumber( e.target.value, 2 );
        const key_name = meta[0];
        const tier_num = meta[1];
        let new_tier = this.props.product_modal.pricing_tiers[tier_num];
        new_tier[key_name] = value.toString();

        store.dispatch( updatePriceTier( tier_num, new_tier ) );
    }

    calc_consumer_price( base_price, tax_rate ) {
        let price = parseFloat( base_price );
        let rate = parseFloat( tax_rate );
        return (price * (1 + rate)).toString();
    }

    calc_base_price( consumer_price, tax_rate ) {
        let price = parseFloat( consumer_price );
        let rate = parseFloat( tax_rate );
        return (price / (1 + rate)).toString();
    }

    handleGenericPricingTierChange( e ) {
        const meta = e.target.id.split("|");
        const value = e.target.value;
        const key_name = meta[0];
        const tier_num = meta[1];
        let new_tier = this.props.product_modal.pricing_tiers[tier_num];
        new_tier[key_name] = value.toString();

        if ( key_name === "base_price" ) {
            new_tier["sales_tax"] = accounting.formatNumber( parseFloat( value ) * parseFloat( this.sales_tax_rate ), 2 );
            const new_consumer_price = this.calc_consumer_price( value, this.sales_tax_rate );
            new_tier["consumer_price"] = accounting.formatNumber( new_consumer_price, 2 );
        }
        else if ( key_name === "consumer_price" ) {
            const new_base_price = this.calc_base_price( value, this.sales_tax_rate );
            new_tier["sales_tax"] = accounting.formatNumber( value - new_base_price , 2 );
            new_tier["base_price"] = accounting.formatNumber( new_base_price, 2 );
        }

        new_tier["price"] = value.toString();

        store.dispatch( updatePriceTier( tier_num, new_tier ) );
    }

    handleGenericPricingTierChangeDate( tier_num, value, formatted_value ) {
        let new_tier = this.props.product_modal.pricing_tiers[tier_num];
        new_tier.start_date = formatted_value || "";
        store.dispatch( updatePriceTier( tier_num, new_tier ) );
    }

    handlePricingTierGradeChange( tier_num, value ) {
        let new_tier = this.props.product_modal.pricing_tiers[tier_num];
        new_tier["grades"] = value;
        store.dispatch( updatePriceTier( tier_num, new_tier ) );
    }

    toISOString( date_string ) {
        if ( date_string === "" ) {
            return "";
        }
        return new Date( date_string ).toISOString();
    }

    render() {
        const available_grades = getGradesSubset( this.props.store_data.grades, this.props.product_modal.grades );
        return (
             <div className="pricing-tiers-wrapper">
                <h4> Pricing </h4>
                 <div className="pricing-tiers">
                    <Row>
                        <span> If the yearbook is ordered after: </span>
                    </Row>
                    <Row>
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th> Start Date </th>
                                    <th> Base Price </th>
                                    <th> Sales Tax(<span dangerouslySetInnerHTML={{ __html: this.sales_tax_rate * 100 }}></span>%) </th>
                                    <th> Consumer Price  </th>
                                    <th> Grades </th>
                                </tr>
                            </thead>
                            <tbody>
                                { this.props.product_modal.pricing_tiers.map( ( tier, i ) => {
                                    return (
                                        <tr key={ i }>
                                            <td>
                                                { i === 0 ? this.props.product_modal.sale_open_date :
                                                    <DatePickerWithId id={ i.toString() } value={ this.toISOString( tier.start_date ) } onChange={ this.handleGenericPricingTierChangeDate } />
                                                }
                                            </td>
                                            <td>
                                                <FormGroup>
                                                    <InputGroup>
                                                        <InputGroup.Addon className="no-shade-addon">$</InputGroup.Addon>
                                                        <FormControl id={ "base_price|" + i } type="text" value={ tier.base_price } onBlur={ this.handleChangeMoney } onChange={ this.handleGenericPricingTierChange } />
                                                    </InputGroup>
                                                </FormGroup>
                                            </td>
                                            <td>
                                                <FormGroup>
                                                    <InputGroup>
                                                        <InputGroup.Addon className="no-shade-addon">$</InputGroup.Addon>
                                                        <FormControl type="text" className="p0 b0" value={ tier.sales_tax } />
                                                    </InputGroup>
                                                </FormGroup>
                                            </td>
                                            <td>
                                                <FormGroup>
                                                    <InputGroup>
                                                        <InputGroup.Addon className="no-shade-addon">$</InputGroup.Addon>
                                                        <FormControl id={ "consumer_price|" + i } type="text" value={ tier.consumer_price }  onBlur={ this.handleChangeMoney } onChange={ this.handleGenericPricingTierChange } />
                                                    </InputGroup>
                                                </FormGroup>
                                            </td>
                                            <td>
                                                <MultiSelectKeyValue items={ available_grades } key_name={ "id" } value_name={ "grade" } selected={ tier.grades } onChange={ ( selected_grades )=>{
                                                        this.handlePricingTierGradeChange( i, selected_grades );
                                                    } } />
                                            </td>
                                        </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </Table>
                    </Row>
                 </div>
             </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        app: state.app,
        store_data: state.store_data,
        product_modal: state.product_modal
    };
}

function mapDispatchToProps(dispatch) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Pricing);
