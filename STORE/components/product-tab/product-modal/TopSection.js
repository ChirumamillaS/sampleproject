import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../../../store.js";
import { Grid, Row, Col, Modal, ControlLabel } from "react-bootstrap";
import DatePicker from "react-bootstrap-date-picker";

// actions
import { genericProductModalUpdate } from "../../../actions/ProductActions.js";

class TopSection extends Component {
    constructor( props ){
        super( props );

        this.handleDateChange = this.handleDateChange.bind(this);
        this.toISOString = this.toISOString.bind(this);
    }

    handleDateChange( key, value ) {
        let date_part = value.split("T")[0];
        store.dispatch( genericProductModalUpdate( key, value ) );

        if( "sale_open_date" == key ) {
            let tempPricingTier = this.props.product_modal.pricing_tiers;
            tempPricingTier[0].start_date = value;
            store.dispatch( genericProductModalUpdate( 'pricing_tiers', tempPricingTier ) );
        }
    }

    toISOString( date_string ) {
        if ( date_string === "" ) {
            return "";
        }
        return new Date( date_string ).toISOString();
    }

    render() {
        let data = this.props.product_modal;
        return (
            <div className="yearbookTopSection">
               <Row>
                   <Col md={ 4 }>
                       <Col componentClass={ ControlLabel } md={ 5 }>
                           Sales Start :
                       </Col>
                       <Col md={ 7 }>
                           <DatePicker value={ this.toISOString(data.sale_open_date) } onChange={ (value, formatted_value) => {
                               this.handleDateChange( "sale_open_date", value );
                           }
                           } />
                       </Col>
                   </Col>
                   <Col md={ 4 }>
                       <Col componentClass={ ControlLabel } md={ 5 }>
                           Final Sales :
                       </Col>
                       <Col md={ 7 }>
                           <DatePicker  value={ this.toISOString(data.sale_close_date) } onChange={ (value, formatted_value) => {
                               this.handleDateChange( "sale_close_date", value );
                           }
                           } />
                       </Col>
                   </Col>
                   <Col md={ 4 }>
                       <Col componentClass={ ControlLabel } md={ 5 }>
                           SKU :
                       </Col>
                       <Col md={ 7 }>
                       </Col>
                   </Col>
               </Row>
               <Row>
                   <Col md={ 4 }>
                       <Col componentClass={ ControlLabel } md={ 5 }>
                           On Campus Orders End :
                       </Col>
                       <Col md={ 7 }>
                           <DatePicker  value={ this.toISOString(data.on_campus_end_date) } onChange={ (value, formatted_value) => {
                               this.handleDateChange( "on_campus_end_date", value );
                           }
                           } />
                       </Col>
                   </Col>
                   <Col md={ 4 }>
                       <Col componentClass={ ControlLabel } md={ 5 }>
                           Sales Channel :
                       </Col>
                       <Col md={ 7 }>
                       </Col>
                   </Col>
               </Row>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        product_modal: state.product_modal
    };
}

function mapDispatchToProps(dispatch) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TopSection);
