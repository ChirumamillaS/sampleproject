import React, { Component } from "react";
import { Grid, Row, Col, Modal, FormControl, CheckboxGroup, Radio, Panel, FormGroup, Checkbox, Textarea, ControlLabel, InputGroup, Form, HelpBlock } from "react-bootstrap";
import store from "../../../store.js";
import { connect } from "react-redux";
import ImageUpload from "../../ImageUpload.js";
import { getGradesSubset } from "../../../helper.js";

//child components
import "../../../../components-web/multi-select/multi-select.less";
import MultiSelectKeyValue from "../../../../components-web/multi-select/multiselect_keyvalue.js";

//actions
import { genericProductModalUpdate } from "../../../actions/ProductActions.js";


class Details extends Component {
    constructor( props ){
        super( props );

        this.handleStoreChange      = this.handleStoreChange.bind(this);
        this.handleStoreChangeInt   = this.handleStoreChangeInt.bind(this);
        this.handleGradeChange      = this.handleGradeChange.bind(this);
        this.handleGenericChange    = this.handleGenericChange.bind(this);
    }

    handleGradeChange( grades ) {
        store.dispatch( genericProductModalUpdate ( "grades", grades ) );
    }

    componentDidMount() {
        const copy_of_grades = Array.from( this.props.store_data.school_grades );
        this.handleGradeChange( copy_of_grades );
    }

    handleGenericChange( name, value ) {
        store.dispatch( genericProductModalUpdate( name, value ) );
    }

    handleStoreChangeInt( e ) {
        let name = e.target.name;
        if ( !name ) {
            name = e.target.id;
        }
        let value = parseInt( e.target.value );
        if ( isNaN( value ) ) {
            value = "";
        }
        store.dispatch( genericProductModalUpdate( name, value ) );
    }

    handleStoreChange( e ) {
        let name = e.target.name;
        if ( !name ) {
            name = e.target.id;
        }
        const value =  e.target.value;
        store.dispatch( genericProductModalUpdate( name, value ) );
    }

    render() {
        const productImageView = this.props.product_modal.store_product_image_type == 0 ?
                                <div className= "productDetailsImage"/>
                             :
                                <ImageUpload type="productimage" imageid={ this.props.product_modal.store_product_image_id } identifier="store_product_image_id" handleChange={ this.handleGenericChange }/>
        const available_grades = getGradesSubset( this.props.store_data.grades, this.props.store_data.school_grades );
        return (
             <div className="yearbook-details-wrapper">
                <h4> Details </h4>
                 <div className="yearbook-details">
                    <Row>
                        <Row>
                            <Col componentClass={ ControlLabel } md={ 3 }>
                                Product Type :
                            </Col>
                            <Col md={ 9 }>
                                <Col md={ 6 }>
                                    <span dangerouslySetInnerHTML={{ __html: "Yearbook" }} />
                                </Col>
                                <Col md={ 6 }>
                                    <HelpBlock className="m0">
                                        <em>(trimSize 8)</em>
                                    </HelpBlock>
                                </Col>
                            </Col>
                        </Row>
                        <Row>
                            <FormGroup>
                                <Col componentClass={ ControlLabel } md={ 3 }>
                                    Name :
                                </Col>
                                <Col md={ 9 }>
                                    <FormControl className="width-275" id="name" type="text" name="store_product_name" value={ this.props.product_modal.store_product_name } placeholder="" onChange={ this.handleStoreChange } />
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup>
                                <Col componentClass={ ControlLabel } md={ 3 }>
                                    Description :
                                </Col>
                                <Col md={ 9 }>
                                    <FormControl id="description" componentClass="textarea" name="store_product_description" value={ this.props.product_modal.store_product_description } placeholder="" className="input-lg" onChange={ this.handleStoreChange } />
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup>
                                <Col componentClass={ ControlLabel } md={ 3 }>
                                    Product Image :
                                </Col>
                                <Col md={ 9 }>
                                    <Row>
                                        <Radio id="default" inline name="store_product_image_type" value={ 0 } checked={ this.props.product_modal.store_product_image_type == 0 } onChange={ ( e ) => { this.handleStoreChangeInt( e ) } }>Default</Radio>
                                        <Radio id="custom" inline name="store_product_image_type" value={ 1 } checked={ this.props.product_modal.store_product_image_type == 1 } onChange={ ( e ) => { this.handleStoreChangeInt( e ) } }>Custom</Radio>
                                    </Row>
                                    <Row>
                                        { productImageView }
                                    </Row>
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup>
                                <Col componentClass={ ControlLabel } md={ 3 }>
                                    Grades :
                                </Col>
                                <Col md={ 9 }>
                                    <MultiSelectKeyValue items={ available_grades } key_name={ "id" } value_name={ "grade" } selected={ this.props.product_modal.grades } onChange={ this.handleGradeChange } />
                                </Col>
                            </FormGroup>
                        </Row>
                        <Row>
                            <FormGroup>
                                <Col className="no-wrap" componentClass={ ControlLabel } md={ 3 }>
                                    Quantity Available :
                                </Col>
                                <Col md={ 9 }>
                                    <FormControl className="width-150" id="quantityAvailable" name = "quantity" value={ this.props.product_modal.quantity } placeholder="" onChange={ this.handleStoreChangeInt } />
                                </Col>
                            </FormGroup>
                        </Row>
                    </Row>
                </div>
            </div>

        );
    }
}

function mapStateToProps(state) {
    return {
        //Project Object is set to this component props
        product_modal : state.product_modal,
        store_data: state.store_data,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Details);
