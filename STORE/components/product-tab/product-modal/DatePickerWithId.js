import React, { Component } from 'react';
import DatePicker from 'react-bootstrap-date-picker';

class DatePickerWithId extends Component {
    constructor( props ){
        super( props );
        this.handleOnChange = this.handleOnChange.bind(this);
    }

    handleOnChange(value, formatted_value) {
        return this.props.onChange(this.props.id, value, formatted_value);
    }

    render() {
        return ( 
            <DatePicker 
                id={ this.props.id }
                value={ this.props.value } 
                onChange={ this.handleOnChange }
            />
        );
    }
}

module.exports = {
    DatePickerWithId
};
