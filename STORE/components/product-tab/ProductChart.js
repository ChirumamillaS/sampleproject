import React, { Component } from 'react';
import { connect } from "react-redux";
import { Grid, Row, Col, Button, ControlLabel, Table, HelpBlock } from 'react-bootstrap';

class ProductChart extends Component {
    render() {
        return (
            <div className="product-enabled-wrapper">
                <Row>
                    <Col componentClass={ ControlLabel } md={ 3 }>
                        Products Enabled
                    </Col>
                    <Col className="text-align-right" md={ 9 }>
                        <Button>Add New Product</Button>
                        <Button className="m-r0">Select from Available Products</Button>
                    </Col>
                </Row>
                <div className="product-enabled">
                    <Row>
                        <Table responsive>
                            <thead>
                                <tr>
                                    <th> Product Name </th>
                                    <th> Grades </th>
                                    <th> Comsumer Price </th>
                                    <th> Sales Start Date </th>
                                    <th> Sales Channel </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2"> <i className="fa fa-window-minimize" aria-hidden="true"></i> Yearbooks and Personalizations </td>
                                    <td colspan="4"> <HelpBlock> <em> (Trim Size 8, Personalization - Nameplates, Cover Type - Hardcover) </em> </HelpBlock> </td>
                                </tr>
                                <tr>
                                    <td href="" dangerouslySetInnerHTML={{ __html: this.props.store_data.products[0][ "store_product_name" ] }}></td>
                                    <td> </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </Table>
                    </Row>
                </div>
            </div>
        );
    }
}

//single argument of the entire Redux store’s state and returns an object to be passed as props.
function mapStateToProps( state ) {
    return {
        //Project Object is set to this component props
        app: state.app,
        store_data: state.store_data,
        product_modal: state.product_modal
    };
}

function mapDispatchToProps( dispatch ) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( ProductChart );
