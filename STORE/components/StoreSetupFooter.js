import { Grid, Row, Col, Modal, Button } from "react-bootstrap";

const StoreSetupFooter = (props) => {

    const hideBack = props.hideBack;
    const hideNext = props.hideNext;
    const hideConfirm = props.hideConfirm;

    return (
        <Row>
            <Col sm={ 6 }>
                { hideBack ? '' : <Button onClick={ props.onBackClick }>Back</Button> }
            </Col>
            <Col sm={ 6 } className="text-align-right">
                <Button onClick={ props.onSaveClick }>Save</Button>
                { hideNext ? '' : <Button onClick={ props.onNextClick }>Next</Button> }
                { hideConfirm ? '' : <Button onClick={ props.onConfirmSubmit }>Confirm &amp; Submit</Button> }
            </Col>
        </Row>
    );
}

module.exports = StoreSetupFooter;
