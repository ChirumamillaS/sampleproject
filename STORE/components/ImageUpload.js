import React, { Component } from 'react';
import { Grid, Row, Col, Modal } from 'react-bootstrap';

class ImageUpload extends Component {
    constructor( props ) {
        super( props )

        //The SAVE URL needs to be passed into component..
        let saveURL = "/sbapi/storesetup/stores/image/";
        let getURL = "/sbapi/storesetup/stores/image/" + this.props.imageid;

        //If we have an ID for a image, make a request to image repo to bring back the image blob..
        if( this.props.imageid ) {
            this.makeAPIRequest( getURL, "GET", null );
        }

        //bind the functions in this component.
        this.handleAccept = this.handleAccept.bind( this );
        this.makeAPIRequest = this.makeAPIRequest.bind( this );
        this.handleCancel = this.handleCancel.bind( this );
        this.handleRemove = this.handleRemove.bind( this );
        this.handleImage = this.handleImage.bind( this );

        //Set DEFAULT states..
        this.state = {
            imagePreview: false,
            hasImage: false,
            uploading: false,
            saveURL: saveURL,
            getURL: getURL
        }
    }

    handleAccept() {
        let url = this.state.saveURL;
        let method = 'POST';
        let image = JSON.stringify( { "image_base_64": this.state.imagePreview } );
        this.makeAPIRequest( url, method, image );
    }

    handleCancel() {
        //If the user hits cancel after a file upload, it will erase the image preview and show the file form again..
        this.setState( {
            imagePreview: false,
            hasImage: false,
            uploading: false,
        } )
    }

    handleRemove() {
        if ( confirm( 'Are You Sure You Would Like To Remove Web Image?' ) ) {
            this.setState( {
                hasImage: false,
                uploading: false,
                imagePreview: null
            } )
            this.props.handleChange( this.props.identifier, null )
        }
    }

    handleImage( event ) {
        let files = event.target.files;

        //If we have files..
        if ( 0 < files.length ) {
            //Get the first one from the files array..
            let fileToLoad = files[ 0 ];

            //if we have a file, then continue.
            if( fileToLoad ) {

                //Confirm file is less than 25mb upload limitation.
                if( fileToLoad.size > 25*1000000 ) {
                    alert( 'Max Image Filesize: 25MB' );
                    return;
                }

                //set the uploading state to true so we can show uploading message.
                this.setState( {
                    uploading: true
                }, function () {
                    //hide the image preview placeholder until file is done..
                    document.getElementById( 'imagePreview' ).style.display = 'none';
                });

                //File reader is to actually go through the file and get its contents.
                let fileReader = new FileReader();
                fileReader.onload = function( fileLoadedEvent ) {
                    if( fileLoadedEvent.target.result ) {
                        //show the image preview box again since we hid it earlier when file was uploading..
                        document.getElementById( 'imagePreview' ).style.display = 'block';

                        //after we have read the file, the target.result is the base64 encoding..
                        this.setState( {
                            imagePreview: fileLoadedEvent.target.result,
                        } ) //setting the imagePreview state will store the base64 in the state, and also tell react that the image is ready to be shown in the render function..
                    }
                }.bind( this )

                fileReader.readAsDataURL( fileToLoad );
            }
        }
    }

    makeAPIRequest( url, method, image ) {
        fetch( url, {
            method: method,
            headers: { 'Content-type': 'application/json' },
            credentials: 'include',
            body: image
        })
        .then( function( response ) {
            if ( !response.ok ) {
                throw Error( response.statusText );
            }
            return response.json();
        } )
        .then( function( responseJson ) {
            if( 'GET' == method ) {
                //if we have a success from the API, then change the state to saved..
                if( responseJson ) {
                    this.setState( {
                        hasImage: true,
                        uploading: false,
                        imagePreview: responseJson
                    } )
                }
            } else if( 'POST' == method ) {
                //if we have a success from the API, then change the state to saved..
                if( responseJson ) {
                    this.setState( {
                        hasImage: true,
                        uploading: false
                    } )
                    this.props.handleChange( this.props.identifier, responseJson )
                }
            }
        }.bind(this) )
        .catch( function( error ) {
            alert( 'Error With Image Upload. Please Try Again.' );
        })
    }

    render() {
        return (
            <div>
                <Row>
                    <Col md={ 12 }>
                        {
                            this.state.uploading ?
                                <Row>
                                    <Col md={ 12 }>
                                        <img width="100%" id="imagePreview" src={ this.state.imagePreview } />
                                    </Col>
                                </Row>
                            :
                                ''
                        }

                        <div className="mp-drag-drop-wrapper">
                            {
                                this.state.uploading ?
                                    this.state.imagePreview ?
                                        <Row>
                                            <Col md={ 12 } className="mp-drag-drop-buttons-wrapper">
                                                <div>
                                                    <div className="btn btn-primary" onClick={ this.handleAccept }>Accept</div>
                                                    <div className="btn btn-secondary" onClick={ this.handleCancel }>Cancel</div>
                                                </div>
                                            </Col>
                                        </Row>
                                    :
                                        <Row>
                                            <Col md={ 12 } className="mp-drag-drop-buttons-wrapper">
                                                <div>
                                                    <div>uploading...</div>
                                                </div>
                                            </Col>
                                        </Row>
                                :
                                    this.state.hasImage ?
                                        <Row>
                                            <Col md={ 12 }>
                                                <img width="100%" id="imagePreview" src={ this.state.imagePreview } />
                                            </Col>
                                            <Col md={ 12 }>
                                                <div className="btn btn-secondary" onClick={ this.handleRemove }>Remove</div>
                                            </Col>
                                        </Row>
                                    :
                                        <div className="mp-drag-drop">
                                            <div className="mp-drag-drop-caption-wrapper">
                                                <div className="mp-drag-drop-caption">
                                                    Drag and drop here to upload OR <a href="javascript:;">browse</a>
                                                </div>
                                                <input type="file"
                                                    name="upfile"
                                                    id="sm-upfile"
                                                    className="mp-drag-drop-input"
                                                    accept="image/*"
                                                    onChange={ this.handleImage }
                                                    value={ this.props.input_file }></input>
                                            </div>
                                        </div>
                                }
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

module.exports = ImageUpload;
