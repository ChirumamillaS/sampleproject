/************************
  * Component Name       : General
  * General.js file create/loads General Tab content for the user.
  ***********************/

// vendor
import React, { Component } from "react";
import { connect } from "react-redux";
import store from "../store.js";
import accounting from "accounting";
import { Grid, Row, Col, Modal, FormControl, CheckboxGroup, Radio, Panel, FormGroup, Checkbox, Textarea, ControlLabel, InputGroup, Form, HelpBlock } from "react-bootstrap";
import ImageUpload from "./ImageUpload.js";
import { makeAPIRequest } from "../helper.js";

// actions
import { updateStoreDataAll, updateSelectedTab, enableProductTab, genericStoreUpdate } from "../actions/AppActions.js";
import { isAlertVisible, setAlertText, setAlertType } from "../actions/AlertActions.js";

// child components
import SalesTax from "./general-tab/SalesTax.js";
import Grades from "./general-tab/Grades.js";
import DeliverySeason from "./general-tab/DeliverySeason.js";
import DistributionDay from "./general-tab/DistributionDay.js";

 // footer
import StoreSetupFooter from "../components/StoreSetupFooter.js";

class General extends Component {

    constructor( props ) {
        super( props );

        this.validate    = this.validate.bind(this);
        this.shippingFee = this.shippingFee.bind(this);
        this.handleSave  = this.handleSave.bind(this);
        this.handleNext  = this.handleNext.bind(this);
        this.handleBack  = this.handleBack.bind(this);
        this.handleStoreChange = this.handleStoreChange.bind(this);
        this.handleStoreChangeInt = this.handleStoreChangeInt.bind(this);
        this.handleChangeMoney = this.handleChangeMoney.bind(this);
        this.handleStoreGradeChange  = this.handleStoreGradeChange.bind(this);
        this.handleShippingChange = this.handleShippingChange.bind(this);
        this.handleCollectTaxChange = this.handleCollectTaxChange.bind(this);
    }


    validate(){
        store.dispatch( enableProductTab( true ) );
        return true;
    }

    shippingFee() {
        // Show only when "Home" shipping is selected.
        if ( [2, 3].includes(this.props.store_data.store_shipping_id) )
            return (
                <FormControl inline type="text"
                                name="shipping_fee"
                                placeholder="$"
                                onChange={ this.handleStoreChange }
                                onBlur={ this.handleChangeMoney }
                                value={ this.props.store_data.shipping_fee }/>
            );
        return '';
    }

    handleCollectTaxChange( e ) {
        let value = parseInt( e.target.value );
        if ( value === 1 && [1, 2].includes( this.props.store_data.store_program_id ) ) {
            store.dispatch( genericStoreUpdate( 'price_include_taxes', 1 ) );
        }
        this.handleStoreChangeInt( e );
    }

    handleSave() {
        makeAPIRequest( "savestore", this.props.store_data );
    }

    handleNext() {
        if ( !this.validate() ) {
            store.dispatch( setAlertText( "Please enter the required fields" ) );
            store.dispatch( setAlertType( "warning" ) );
            store.dispatch( isAlertVisible( true ) );
            return false;
        }

        store.dispatch( isAlertVisible( false ) );

        store.dispatch( updateSelectedTab( "productPricing" ) );
        this.handleSave();
    }

    handleBack() {
        store.dispatch( updateSelectedTab( "program" ) );
    }

    handleStoreGradeChange( grades ) {
        store.dispatch( genericStoreUpdate( "school_grades", grades ) );
    }

    handleShippingChange ( e ) {
        let currentVal = this.props.store_data.store_shipping_id || 0;
        let value = parseInt(e.target.value);
        if ( value === currentVal ) {
            value = null;
        }
        else if ( currentVal === 3 ) {
            value = currentVal - value;
        }
        else {
            value = currentVal + value;
        }

        store.dispatch( genericStoreUpdate( "store_shipping_id", value ) );
    }

    handleStoreChangeInt( e ) {
        let name = e.target.name;
        if ( !name ) {
            name = e.target.id;
        }
        let value = parseInt( e.target.value );
        if ( isNaN( value ) ) {
            value = "";
        }
        store.dispatch( genericStoreUpdate( name, value ) );
    }

    handleChangeMoney( e ) {
        let value = accounting.formatNumber(e.target.value, 2);
        store.dispatch( genericStoreUpdate( 'shipping_fee', value ) );
    }

    handleStoreChange( e ) {
        let name = e.target.name;
        if ( !name ) {
            name = e.target.id;
        }
        const value =  e.target.value;
        store.dispatch( genericStoreUpdate( name, value ) );
    }

    handleGenericChange( name, value ) {
        store.dispatch( genericStoreUpdate( name, value ) );
    }


    render() {
        return (
            <div className="store-setup-tab-wrapper">
                <div className="store-setup-tab-general">
                    <Form horizontal>
                        {/*<!--Store Type-->*/}
                        <FormGroup controlId="storeType">
                            <Col componentClass={ ControlLabel } md={ 2 }>
                                Store Type:
                            </Col>
                            <Col md={ 10 }>
                                <Row>
                                    <Col md={ 3 }>
                                        <Radio inline value={ 1 } checked={ this.props.store_data.store_type_id === 1 } onChange={ this.handleStoreChangeInt } name="store_type_id">Yearbook, Products and Ads</Radio>
                                    </Col>
                                    <Col md={ 2 }>
                                        <Radio inline value={ 2 } checked={ this.props.store_data.store_type_id === 2 } onChange={ this.handleStoreChangeInt } name="store_type_id">Ads Only</Radio>
                                    </Col>
                                    <Col md={ 7 }>
                                        <HelpBlock>
                                            <em>(Select a Store Type to manage products.)</em>
                                        </HelpBlock>
                                    </Col>
                                </Row>
                            </Col>
                        </FormGroup>

                        {/*<!--Sales Tax-->*/}
                        <FormGroup controlId="salesTax">
                            <Col componentClass={ ControlLabel } md={ 2 }>
                                Sales Tax:
                            </Col>
                            <Col md={ 10 }>
                                <Row>
                                    <Col md={ 3 }>
                                        <Radio inline value={ 1 } checked={ this.props.store_data.collect_sales_tax === 1 } onChange={ this.handleCollectTaxChange } name="collect_sales_tax">Yes, Collect Sales Tax</Radio>
                                    </Col>
                                    <Col md={ 2 }>
                                        <Radio inline value={ 0 } checked={ this.props.store_data.collect_sales_tax === 0 } onChange={ this.handleCollectTaxChange } name="collect_sales_tax">No Sales Tax</Radio>
                                    </Col>
                                    <Col md={ 7 }>
                                        <HelpBlock>
                                            <em>(Choose Yes to have Balfour collect sales tax. If you have questions, please contact your representative.)</em>
                                        </HelpBlock>
                                    </Col>
                                </Row>
                                {
                                    this.props.store_data.collect_sales_tax ?
                                    <Row>
                                        <SalesTax { ...this.props.store_data } />
                                    </Row>
                                    :
                                        ""
                                }
                            </Col>
                        </FormGroup>

                        {/*<!--Grades-->*/}
                        <FormGroup controlId="grades">
                            <Col componentClass={ ControlLabel } md={ 2 }>
                                Grades:
                            </Col>
                            <Col md={ 4 }>
                                <Grades handleStoreGradeChange = {this.handleStoreGradeChange} selectedGrades={ this.props.store_data.school_grades }  availableGrades={ this.props.store_data.grades }/>
                            </Col>
                            <Col md={ 6 }>
                            </Col>
                        </FormGroup>

                        {/*<!--Enrollment-->*/}
                        <FormGroup controlId="enrollment">
                            <Col componentClass={ ControlLabel } md={ 2 }>
                                Enrollment:
                            </Col>
                            <Col md={ 2 }>
                                <FormControl id="enrollment" type="text" value={ this.props.store_data.enrollment } onChange={ this.handleStoreChangeInt } placeholder="" />
                            </Col>
                            <Col md={ 8 }>
                            </Col>
                        </FormGroup>

                        {/*<!--Ship To-->*/}
                        <FormGroup inline controlId="shipTo">
                            <Col className="p5" componentClass={ ControlLabel } md={ 2 }>
                                Ship To:
                            </Col>
                            <Col md={ 3 }>
                                <Checkbox name="store_shipping_id" value={ 1 } checked={ [1, 3].includes(this.props.store_data.store_shipping_id) } onChange={ this.handleShippingChange } inline>Schools</Checkbox>
                                <Checkbox name="store_shipping_id" value={ 2 } checked={ [2, 3].includes(this.props.store_data.store_shipping_id) } onChange={ this.handleShippingChange } inline>Home</Checkbox>
                            </Col>
                            <Col md={ 4 }>
                                { this.shippingFee() }
                            </Col>
                        </FormGroup>

                        {/*<!--Delivery Season-->*/}
                        <DeliverySeason { ...this.props.store_data }/>

                        {/*<!--Distribution Label-->*/}
                        <FormGroup inline controlId="distributionDayLabel">
                            <Col componentClass={ ControlLabel } md={ 2 }>
                                Distribution Day Label:
                                <em className="display-block">(optional)</em>
                            </Col>
                            <Col componentClass={ ControlLabel } md={ 10 }>
                            </Col>
                        </FormGroup>

                        {/*<!--Web Message-->*/}
                        <FormGroup controlId="deliverySeason">
                            <Col componentClass={ ControlLabel } md={ 2 }>
                                Web Message:
                                <em className="display-block">(optional)</em>
                            </Col>
                            <Col md={ 4 }>
                                <FormControl componentClass="textarea" name="web_message" value={ this.props.store_data.web_message } onChange={ this.handleStoreChange } />
                            </Col>
                            <Col md={ 6 }>
                                <HelpBlock>
                                    <em>(Create a custom message to your students.)</em>
                                </HelpBlock>
                            </Col>
                        </FormGroup>

                        {/*<!--Web Image-->*/}
                        {
                            this.props.store_data.store_id > 0 ?
                                <FormGroup controlId="webImage">
                                    <Col componentClass={ ControlLabel } md={ 2 }>
                                        Web Image:
                                        <em>(optional)</em>
                                    </Col>
                                    <Col md={ 6 }>
                                        <ImageUpload imageid={ this.props.store_data.web_image_id } identifier="web_image_id" handleChange={ this.handleGenericChange }/>
                                    </Col>
                                    <Col md={ 4 }>
                                        <HelpBlock>
                                            <em>(Upload an image for your store front.)</em>
                                        </HelpBlock>
                                    </Col>
                                </FormGroup>
                            :
                                ""
                        }
                    </Form>
              </div>
                <div className="store-setup-footer">
                    <StoreSetupFooter hideConfirm="true" onSaveClick={ this.handleSave } onNextClick={ this.handleNext } onBackClick={ this.handleBack } />
                </div>
          </div>
        );

    }
}

function mapStateToProps( state ) {
    return {
        //Project Object is set to this component props
        general: state.general,
        app: state.app,
        store_data: state.store_data,
    };
}

function mapDispatchToProps( dispatch ) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( General );
