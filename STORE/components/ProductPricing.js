import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Row, Col, Modal } from "react-bootstrap";
import store from "../store.js";

// footer
import StoreSetupFooter from "../components/StoreSetupFooter.js";

// child components
import ProductChart from "./product-tab/ProductChart.js";
import ProductModal from "./product-tab/ProductModal.js";

// actions
import { updateSelectedTab, genericStoreUpdate } from "../actions/AppActions.js";
import { isAlertVisible, setAlertText, setAlertType } from "../actions/AlertActions.js";

class ProductPricing extends Component {

    constructor( props ) {
        super( props );

        this.handleSave = this.handleSave.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.handleConfirm = this.handleConfirm.bind(this);

    }

    handleSave() {
        let storeId = this.props.store_data.store_id;
        var options = {
            method: "PATCH",
            headers: { "Content-type": "application/json" },
            credentials: "include",
            body: JSON.stringify( this.props.store_data ),
        };

        fetch( `/sbapi/storesetup/stores/${storeId}`, options )
        .then( response => {
            if ( !response ) {
                throw Error( response.statusText );
            }

            return response.json();
        })
        .then( responseJson => {
            return true;
        })
        .catch((error) => {
            let alertText = `${error} : Please contact administration.`;
            store.dispatch( setAlertText( alertText ) )
            store.dispatch( setAlertType( "danger" ) );
            store.dispatch( isAlertVisible( true ) )
            return false;
        });
    }

    handleBack() {
        store.dispatch( updateSelectedTab( "general" ) );
    }

    handleConfirm() {
    }

    render() {
        return(
            <div className="store-setup-tab-wrapper">
                <div className="store-setup-tab-product-pricing">
                    { this.props.store_data.products.length > 0 ? <ProductChart /> : <ProductModal store_data={ this.props.store_data } product_modal={ this.props.product_modal } /> }
                </div>
                <div className="store-setup-footer">
                    <StoreSetupFooter hideNext="true" onBackClick={ this.handleBack } onSaveClick={ this.handleSave } onConfirmSubmit={ this.handleConfirm } />
                </div>
            </div>
        );
    }

}

//single argument of the entire Redux store’s state and returns an object to be passed as props.
function mapStateToProps( state ) {
    return {
        //Project Object is set to this component props
        app: state.app,
        store_data: state.store_data,
        product_modal: state.product_modal
    };
}

function mapDispatchToProps( dispatch ) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( ProductPricing );
