/************************
  * Component Name       : Program
  * Program.js file create/loads a store selected by the user.
  ***********************/

// vendor
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, FormGroup, ControlLabel, Radio, Alert, ButtonToolbar, DropdownButton, MenuItem, FormControl } from "react-bootstrap";
import store from "../store.js";
import { makeAPIRequest } from "../helper.js";

// actions
import { updateStoreDataAll, updateSelectedTab, enableGeneralTab } from "../actions/AppActions.js";
import { updateProgramType, updateProgramFee } from "../actions/ProgramActions.js";
import { isAlertVisible, setAlertText, setAlertType } from "../actions/AlertActions.js";

// footer
import StoreSetupFooter from "./StoreSetupFooter.js";

class Program extends Component {

    constructor(props){
        super(props);

        this.validate = this.validate.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handleProgramSelected = this.handleProgramSelected.bind(this);
        this.createNewStore = this.createNewStore.bind(this);
        this.feeOptions = this.feeOptions.bind(this);
    }

    validate() {
        let radioButtons = document.getElementsByName( "programTab" );
        for ( let radioButton of radioButtons ) {
            if( radioButton.checked ) {
                store.dispatch( enableGeneralTab( true ) );
                return true;
            }
        }
        return false;
    }

    createNewStore() {
        makeAPIRequest( "createstore", this.props.store_data );
    }

    handleSave() {
        if ( !this.validate() ) {
            store.dispatch( setAlertText( "Please choose an option for your school" ) );
            store.dispatch( setAlertType( "warning" ) );
            store.dispatch( isAlertVisible( true ) );
            return false;
        }
        store.dispatch( isAlertVisible( false ) );

        let storeId = this.props.store_data.store_id;
        if ( storeId === 0 ) {
            this.createNewStore();
        }
        else {
            makeAPIRequest( "savestore", this.props.store_data );
        }
        return true;
    }

    handleNext() {

        if( this.handleSave() ) {
            store.dispatch( isAlertVisible( false ) );
            store.dispatch( updateSelectedTab( 'general' ) );
        }
        else {
            store.dispatch( setAlertText( "Please choose an option for your school" ) );
            store.dispatch( setAlertType( "warning" ) );
            store.dispatch( isAlertVisible( true ) );
            return false;
        }
    }

    handleProgramSelected( e ) {
        store.dispatch( updateProgramType( parseInt( e.target.value) ) );
        store.dispatch( isAlertVisible( false ) );
    }

    handleFeeSelected(e) {
        const fee_id = parseInt( e.target.value );
        store.dispatch( updateProgramFee( fee_id ) );
    }

    feeOptions() {
        if ( this.props.app.store_defaults.advancePermission === "1" ) {
            return (
                        <Row>
                            <Col componentClass={ ControlLabel } md={ 1 }>
                                Fee Option :
                            </Col>
                            <Col md={ 11 }>
                                <FormControl componentClass="select" placeholder="Select Fee Option" value={ this.props.store_data.store_program_fee_option_id } onChange={ this.handleFeeSelected } id="programFeeOptions">
                                    <option value={ null }>Select a Fee Option</option>
                                    {
                                        this.props.store_data.program_fee_options.map((item) => {
                                            if ( item.store_program_id === this.props.store_data.store_program_id ){
                                                return <option value={ item.store_program_fee_option_id } key={ item.store_program_fee_option_id } >{ item.store_program_fee_option_name }</option>
                                            }
                                        })

                                    }
                                </FormControl>
                            </Col>
                        </Row>
                    );

        }
        return "";
    }

    render() {
        let program = this.props.store_data;

        return(
            <div className="store-setup-tab-wrapper">
                <div className="store-setup-tab-program">
                    <Row dangerouslySetInnerHTML={{ __html: this.props.app.store_defaults.storeSetupConfig.store_setup_welcome_message }} />
                    <FormGroup>
                        <Row>
                            <Col md={ 4 } className="panel-default" onClick={ () => { store.dispatch(updateProgramType(3)) } } >
                                <div className={ program.store_program_id === 3 ? "panel-heading panel-heading-outline" : "panel-heading" }>
                                    <h3 class="panel-title">{ this.props.app.store_defaults.storeSetupConfig.store_setup_box_1_title }</h3>
                                </div>
                                <div className={ program.store_program_id === 3 ? "panel-body panel-body-outline" : "panel-body" }>
                                    <ul dangerouslySetInnerHTML={{ __html: this.props.app.store_defaults.storeSetupConfig.store_setup_box_1_content }} />
                                </div>
                                <Radio inline value={ 3 } name="programTab" onChange={ this.handleProgramSelected } checked={ program.store_program_id === 3 } />
                            </Col>
                            <Col md={ 4 } className="panel-default" onClick={ () => { store.dispatch(updateProgramType(2)) } }>
                                <div className={ program.store_program_id === 2 ? "panel-heading panel-heading-outline" : "panel-heading" }>
                                    <h3 class="panel-title">{ this.props.app.store_defaults.storeSetupConfig.store_setup_box_2_title }</h3>
                                </div>
                                <div className={ program.store_program_id === 2 ? "panel-body panel-body-outline" : "panel-body" }>
                                    <ul dangerouslySetInnerHTML={{ __html: this.props.app.store_defaults.storeSetupConfig.store_setup_box_2_content }} />
                                </div>
                                <Radio inline value={ 2 } name="programTab" onChange={ this.handleProgramSelected } checked={ program.store_program_id === 2 } />
                            </Col>
                            <Col md={ 4 } className="panel-default" onClick={ () => { store.dispatch(updateProgramType(1)) } }>
                                <div className={ program.store_program_id === 1 ? "panel-heading panel-heading-outline" : "panel-heading" }>
                                    <h3 class="panel-title">{ this.props.app.store_defaults.storeSetupConfig.store_setup_box_3_title }</h3>
                                </div>
                                <div className={ program.store_program_id === 1 ? "panel-body panel-body-outline" : "panel-body" }>
                                    <ul dangerouslySetInnerHTML={{ __html: this.props.app.store_defaults.storeSetupConfig.store_setup_box_3_content }} />
                                </div>
                                <Radio inline value={ 1 } name="programTab" onChange={ this.handleProgramSelected } checked={ program.store_program_id === 1 } />
                            </Col>
                        </Row>
                        {
                            this.feeOptions()
                        }
                    </FormGroup>
                    <Row className="store-setup-footer">
                        <StoreSetupFooter hideBack="true" hideConfirm="true" onSaveClick={ this.handleSave } onNextClick={ this.handleNext } />
                    </Row>
                </div>
            </div>
        );
    }
}

//single argument of the entire Redux store’s state and returns an object to be passed as props.
function mapStateToProps( state ) {
    return {
        //Project Object is set to this component props
        program : state.program,
        app: state.app,
        store_data: state.store_data,
    };
}

function mapDispatchToProps( dispatch ) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( Program );
