// actions
import { UPDATE_PRICE_TIER, UPDATE_PRODUCT_ALL } from "../actions/ProductActions.js";

const initialState = {
    store_product_id: null,
    store_product_sku: "ABC1234",
    master_product_id: "",
    product_category_id: 1,
    store_product_name: "",
    store_product_description: "",
    store_product_image_id: null,
    store_product_image_type: "1",
    store_id: "",
    product_template_id: null,
    sale_channel_id: "",
    sale_open_date: "",
    grades: [],
    sale_close_date: "",
    on_campus_end_date: "",
    quantity: "",
    cover_type_id: null,
    personalization_type_id: null,
    personalization_end_date: "",
    parent_id: null,
    pricing_tiers: [
        {
            start_date: "",
            base_price: "",
            price: "",
            sales_tax: "",
            consumer_price: "",
            grades: []
        },
        {
            start_date: "",
            base_price: "",
            price: "",
            sales_tax: "",
            consumer_price: "",
            grades: []
        },
        {
            start_date: "",
            base_price: "",
            price: "",
            sales_tax: "",
            consumer_price: "",
            grades: []
        },
        {
            start_date: "",
            base_price: "",
            price: "",
            sales_tax: "",
            consumer_price: "",
            grades: []
        },
        {
            start_date: "",
            base_price: "",
            price: "",
            sales_tax: "",
            consumer_price: "",
            grades: []
        },
        {
            start_date: "",
            base_price: "",
            price: "",
            sales_tax: "",
            consumer_price: "",
            grades: []
        },
    ]
}

function product_modal (state: State = initialState, action: Action): State {
    switch(action.type) {
        case "GENERIC_PRODUCT_MODAL_UPDATE":
            let new_value = {};
            new_value[action.name] = action.value;
            return Object.assign(
                {}, state, new_value
            );
	    case UPDATE_PRICE_TIER:
            let new_pricing_tiers = state.pricing_tiers;
            new_pricing_tiers[action.tier_num] = action.tier_data;

			return Object.assign(
				{}, state, {
					pricing_tiers: new_pricing_tiers
				}
			);

        case UPDATE_PRODUCT_ALL:
            return Object.assign( {}, state, action.data );

        default :
            return state;
    }
}

module.exports = {
    product_modal
};
