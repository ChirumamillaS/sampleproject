import { SELECTED_TAB, UPDATE_DEFAULTS, STORE_STATUS, ENABLE_GENERAL_TAB, ENABLE_PRODUCT_TAB } from '../actions/AppActions.js';

const wp_vars = {
   "storeSetupConfig":{},
   "isCSR":"",
   "advancePermission":"",
   "currentProjectNumber":"",
   "currentProjectName":"",
   "projectID":"",
   "storeID":"",
};

const initialState = {
    selected_tab        : 'program',
    general_enabled     : false,
    product_enabled    :  false,
    store_status        : 'new', // TODO: This has to be replaced, store_data has a store_status_id that should be used.
    store_defaults      : wp_vars
};

function app(state: State = initialState, action: Action): State {
	switch(action.type) {
		case SELECTED_TAB 	:
			return Object.assign(
				{}, state, {
					selected_tab: action.data
				}
			);
        case UPDATE_DEFAULTS    :
            return Object.assign(
                {}, state, {
					store_defaults: action.data
				}
            );
		case STORE_STATUS 	:
			return Object.assign(
				{}, state, {
					store_status: action.data
				}
			);
		case ENABLE_GENERAL_TAB	:
			return Object.assign(
				{}, state, {
					general_enabled: action.data
				}
			);
		case ENABLE_PRODUCT_TAB	:
			return Object.assign(
				{}, state, {
					product_enabled: action.data
				}
			);
		default :
			return state;
    }
}

module.exports = {
    app
};
