import { ALERT_VISIBLE, ALERT_TEXT, ALERT_TYPE } from '../actions/AlertActions.js';

const initialState = {
    alertVisible    : false,
    alertText       : '',
    alertType       : ''
}

function alert(state: State = initialState, action: Action): State {
	/*  Async Calls Happen here and Data is fetched from rem
	ote server
	until server is up and running using a local json file to populate the data */

	switch(action.type) {
		case ALERT_VISIBLE 	:
			return Object.assign(
				{}, state, {
					alertVisible: action.data
				}
			);
		case ALERT_TEXT 	:
			return Object.assign(
				{}, state, {
					alertText: action.data
				}
			);
		case ALERT_TYPE 	:
            //"success", "warning", "danger", "info" 
			return Object.assign(
				{}, state, {
					alertType: action.data
				}
			);
		default :
			return state;
		}
}

module.exports = {
    alert
};
