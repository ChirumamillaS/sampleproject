const initialState = {
}

function program(state: State = initialState, action: Action): State {

	switch(action.type) {
		default :
			return state;
		}
}

module.exports = {
    program
};
