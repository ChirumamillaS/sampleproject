import store from "./store.js";
import { isAlertVisible, setAlertText, setAlertType } from "./actions/AlertActions.js";
import { updateStoreDataAll, updateDefaults } from "./actions/AppActions.js";
import { updateProductAll } from "./actions/ProductActions.js";

function getGradesSubset(all_values, filter) {
    if ( !filter ) {
        return all_values;
    }

    let filtered_grades = [];

    all_values.map( grade => {
        if ( filter.includes( grade.id ) ) {
            filtered_grades.push( grade );
        }
    });
    return filtered_grades;
}

function removeNullFromObject( key_value_object ) {
    Object.keys( key_value_object ).forEach( key => {
        if ( key_value_object[ key ] === null ) {
            key_value_object[ key ] = "";
        }
    });
    return key_value_object;
}

//custom can be anything you want, for special situations..
function makeAPIRequest( type, data, custom ) {
    let url = "", payload = "", method = "";

    switch( type ) {
        case "getdefaults":
            url = '/sbapi/storesetup/defaults';
            method = "GET";
            break;
        case "createstore":
            url = `/sbapi/storesetup/new`
            payload = JSON.stringify( data );
            method = "POST";
            break;
        case "savestore":
            url = `/sbapi/storesetup/stores/${data.store_id}`
            payload = JSON.stringify( data );
            method = "PATCH";
            break;
        case "createproduct":
            url = `/sbapi/storesetup/stores/${ data.store_id }/products/new`;
            payload = JSON.stringify( custom );
            method = "POST";
            break;
        case "saveproduct":
            url = `/sbapi/storesetup/stores/${ data.store_id }/products/${ custom.store_product_id }`;
            payload = JSON.stringify( custom );
            method = "PATCH";
            break;
    }


    var options = {
        method: method,
        headers: { "Content-type": "application/json" },
        credentials: "include",
        body: payload,
    };

    if( "GET" === method ) {
        delete options.body;
    }

    fetch( url, options )
    .then( response => {
        if ( !response ) {
            throw Error( response.statusText );
        }
        return response.json();
    })
    .then( responseJson => {
        switch( type ) {
            case "getdefaults" :
                store.dispatch( updateDefaults( responseJson ) );
                break;
            case "savestore":
                store.dispatch( updateStoreDataAll( responseJson ) );
                store.dispatch( setAlertText( "Data Saved" ) )
                store.dispatch( setAlertType( "success" ) );
                store.dispatch( isAlertVisible( true ) )
                break;
            case "createstore":
                store.dispatch( updateStoreDataAll( responseJson ) );
                break;
            case ( "createproduct" || "saveproduct" ):
                const saved_product = responseJson.createdProduct;
                delete responseJson["createdProduct"];
                store.dispatch( updateProductAll( saved_product ) );
                store.dispatch( updateStoreDataAll( responseJson ) );
                break;
        }
    })
    .catch( error => {
        let alertText = error + " : Please contact administration.";
        store.dispatch( setAlertText( alertText ) )
        store.dispatch( setAlertType( "danger" ) );
        store.dispatch( isAlertVisible( true ) )
        return false;
    });
}

module.exports = {
    getGradesSubset,
    removeNullFromObject,
    makeAPIRequest
};
