import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import React from 'react';
import store from './store.js';
import App from './containers/App.js';

ReactDOM.render(
    <Provider store={ store }>
        <App />
    </Provider>,

    document.getElementById('root')
);
