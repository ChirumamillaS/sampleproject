/************************
 * Component Name       : App
 * This App.js creates the landing tabs for "store setup edit" page.
 ***********************/

import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { PageHeader, Tab, Alert, Nav, NavItem, Row } from 'react-bootstrap';
import store from '../store.js';
import { makeAPIRequest } from "../helper.js";

// actions
import { updateStoreDataAll, updateSelectedTab, enableGeneralTab, enableProductTab } from '../actions/AppActions.js';
import { updateProgramType } from '../actions/ProgramActions.js';
import { isAlertVisible, setAlertText, setAlertType } from "../actions/AlertActions.js";

// child components
import Program from '../components/Program.js';
import General from '../components/General.js';
import ProductPricing from '../components/ProductPricing.js';

class App extends Component {
    constructor(props){

        super(props);

        this.handleAlertDismiss     = this.handleAlertDismiss.bind(this);
        this.makeGetAPICall          = this.makeGetAPICall.bind(this);
        this.updateTab              = this.updateTab.bind(this);

        //get store data if there is store ID existing
        if(this.props.app.store_defaults.storeID) {
            let getURL = `/sbapi/storesetup/stores/${ this.props.app.store_defaults.storeID }`;
            this.makeGetAPICall( getURL );
        }

        //get default values such as grades and program fee options
        makeAPIRequest( "getdefaults" );
    }

    makeGetAPICall( getURL ) {
        fetch(getURL, {
            method: 'get',
            headers: {'Content-type': 'application/json'},
            credentials: 'include'
        })
        .then((response) => {
            if (!response.ok){
                //error
                throw Error(response.statusText);
            }
            return response.json();
        })
        .then((responseJson) => {
            store.dispatch(updateStoreDataAll(responseJson));
            return true;
        })
        .catch((error) => {
            let alertText = error + " : Please contact administration.";
            if ( error.message.includes( "Unauthorized" ) ) {
                alertText = "Unauthorized! Your session may be expired. ";
            }
            store.dispatch( setAlertText( alertText ) )
            store.dispatch( setAlertType( "danger" ) );
            store.dispatch( isAlertVisible( true ) )
            return false;
        });
    }

    handleAlertDismiss() {
        store.dispatch(isAlertVisible(false));
    }

    updateTab(e) {
        store.dispatch(updateSelectedTab(e));
    }

    render(){
        let appCurrentState     = store.getState().app;
        let alertCurrentState  = store.getState().alert;

        return (
            <div className="container-fluid">
                <div className="storesetup-edit">
                    <PageHeader>{ this.props.app.store_defaults.currentProjectName }</PageHeader>
                    <div>
                        { alertCurrentState.alertVisible ?
                            <Alert bsStyle={ alertCurrentState.alertType } onDismiss={ this.handleAlertDismiss }>
                                { alertCurrentState.alertText }
                            </Alert>
                            : ''
                        }
                    </div>
                    <Tab.Container id="store-setup-tabs" className="store-setup-tabs" defaultActiveKey={ appCurrentState.selected_tab } activeKey={ appCurrentState.selected_tab } onSelect={ this.updateTab }>
                        <Row className="clearfix">
                            <Nav bsStyle="pills">
                                <NavItem eventKey="program">
                                    { this.props.app.store_defaults.storeSetupConfig.store_setup_tab_1 }
                                </NavItem>
                                <NavItem eventKey="general" disabled={ !appCurrentState.general_enabled } >
                                    { this.props.app.store_defaults.storeSetupConfig.store_setup_tab_2 }
                                </NavItem>
                                <NavItem eventKey="productPricing" disabled={ !appCurrentState.product_enabled } >
                                    { this.props.app.store_defaults.storeSetupConfig.store_setup_tab_3 }
                                </NavItem>
                                <span className="status-store">Store Status: { appCurrentState.store_status }</span>
                            </Nav>
                            <Tab.Content animation>
                                <Tab.Pane eventKey="program">
                                    <Program />
                                </Tab.Pane>
                                <Tab.Pane eventKey="general">
                                    <General />
                                </Tab.Pane>
                                <Tab.Pane eventKey="productPricing">
                                    <ProductPricing />
                                </Tab.Pane>
                            </Tab.Content>
                        </Row>
                    </Tab.Container>
                </div>
            </div>
        );
    }
}

//single argument of the entire Redux store’s state and returns an object to be passed as props.
function mapStateToProps(state) {
    return {
        //Project Object is set to this component props
        app     : state.app,
        project : state.project,
        alert   : state.alert
    };
}

function mapDispatchToProps(dispatch) {
    return {
        //actions: bindActionCreators(AppActions, dispatch)
        // action are set as props to this component
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App, Program);
